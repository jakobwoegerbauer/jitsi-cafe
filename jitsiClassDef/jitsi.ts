import { Message } from "../src/communication";

// this objects and classes are defined in Jitsi-Meet

export class JitsiConference {
    _room: JitsiRoom;
    roomName: string;
    getLocalDisplayName(): string {
        throw new Error("Method not implemented.");
    }
    sendEndpointMessage(destination: string, msg: Message) {
        throw new Error("Method not implemented.");
    }
    removeConferenceListener(arg0: string, _onUserJoined: (id: any, participant: any) => void) {
        throw new Error("Method not implemented.");
    }
    getMyUserId(): string {
        throw new Error("Method not implemented.");
    }
    getParticipantDisplayName(id: string): string {
        throw new Error("Method not implemented.");
    }
    addConferenceListener(arg0: string, _onUserJoined: (id: any, participant: any) => void) {
        throw new Error("Method not implemented.");
    }
}

export class JitsiParticipant {
    APP: any;
    getId(): string {
        throw new Error("Method not implemented.");
    }
    getTracksByMediaType(mediaType: string): JitsiTrack[] {
        throw new Error("Method not implemented.");
    }
}

export class JitsiTrack {
    containers: HTMLMediaElement[];
    audioLevel: number;
}

export class JitsiRoom {
    onRemoteTrackAdded: (trackAddedEvent: any) => void;
    setSubject(arg0: string) {
        throw new Error("Method not implemented.");
    }
    setDisplayName(name: string) {
        throw new Error("Method not implemented.");
    }
    rtc = new JitsiRTC();
    isModerator(): boolean {
        throw new Error("Method not implemented.");
    }
    getParticipants(): JitsiParticipant[] {
        throw new Error("Method not implemented.");
    }
    getLocalAudioTrack(): JitsiTrack {
        throw new Error("Method not implemented.");
    }
    getLocalVideoTrack(): JitsiTrack {
        throw new Error("Method not implemented.");
    }
}

export class JitsiRTC {
    _channel: { isOpen: () => boolean; };
    addListener(eventName: string, listener: () => void) {
        throw new Error("Method not implemented.");
    }
}

export class JitsiStore {
    getState(): any {
        throw new Error("Method not implemented.");
    }
    subscribe(helper: (_: any) => void) {
        throw new Error("Method not implemented.");
    }
};

export class JitsiApp {
    store: JitsiStore;
    conference: JitsiConference
};

function getContainer(track: JitsiTrack): HTMLMediaElement {
    return track?.containers[0];
}

var APP = (window as any).APP as JitsiApp;

export { APP, getContainer };