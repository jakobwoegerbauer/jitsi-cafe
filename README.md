# Jitsi-Café

A small Javascript project which allows Jitsi-Meet users to move around their virtual character. The volumes of other people change according to their position relative to you.

NOTE: This project is still at its very beginning.

# Controls

## Basics

In the "absolute" mode (default) you can move around with your arrow keys on your keyboard and rotate with the '.' and ',' keys.

<img src="img/controls_absolute.svg" width="400"/>

You can change the controls to "view" mode in the Settings. In this mode you can move forward and backwards in the same way but rotate with your left and right arrow keys.

<img src="img/controls_view.svg" width="400"/>

You can zoom in and out by pressing the '+' and '-' keys.

## Settings

You can hide and show the map by clicking on the "Hide" button on the black menu bar. This menu bar is in the top left corner.

You can open and close the settings with the "Settings" button.

### Background color
This option changes the map background color. This is only useful with the 'window' view mode. Default: transparent

### View mode
Switch between fullscreen and window mode. In window mode the map is attached to the menu bar and can be dragged around. In fullscreen mode the map is shown over the full Jitsi-Meet window.

### View size
This option is only available in window mode and changes the size of the window.

### Audio framework
Switch between the available audio frameworks:
* distance: In this mode only the volume of each remote participant is changed based on the distance to the local participant. This is the default mode.
* spatial: This mode uses stereo output to indicate the direction where the sounds are coming from. The volume is changed based on the distance between the participants. Headphones are recommended for the best experience. NOTE: This does not work in all browsers.

### Movement
Switch between the available movement/control modes. See the [Basics](#basics) for more details.

# Setup on your server

Clone this repository:
```console
git clone https://gitlab.com/jakobwoegerbauer/jitsi-cafe.git
```

Build the repository:
```console
npm install
npm run build
```

Copy the `dist/jitsi-cafe.js` file in the `libs` directory of your Jitsi-Meet installation.
Then include the JS script in the `index.html` file at the end of the `<body>` tags like this:
```html
...
  </head>
  <body>
    <!--#include virtual="body.html" -->
    <div id="react"></div>
    <script src="libs/jitsi-cafe.js"></script>
  </body>
</html>
```

On a default installation on Ubuntu/Debian systems you can find the Jitsi-Meet source directory in `/usr/share/jitsi-meet`.

You are ready to go! The canvas will appear after joining a room.

# Development
If you want to improve the project it is the easiest to just use meet.jit.si (or any other Jitsi-Meet server) and copy the whole content of the js file into the browsers console after the page has finished loading. If you change something => reload the page and execute the Javascript code again.

## Roadmap / planned features
* Stereo mode: Participants should be able to hear the direction the sound is coming. A pan node can be used for that. For that feature to be useful, participants have to be able to rotate. Two map modes should be available: a north oriented map and a map which is rotating with the participant.
* Settings UI: Basic settings should be configurable in the UI (movement keys, Stereo mode, map mode)
* Enable/disable for all: Moderators should be able to enable and disable Jitsi-Café for all participants.
* Speaker mode: Moderators should be able to put someone to "speaker mode". Participants on speaker mode should be heard by anyone at full volume.
* Resizeable map view: The size of the canvas should be modifyable by each participant. This is just a zoom and does not affect the positioning or volume.
* Resizable map: Moderators should be able to change the actual map size and/or the maximal hearing distance.
* Moderators: Moderators should be able to give Moderator rights to other participants.