const path = require('path');

module.exports = {
    context: path.resolve(__dirname),
    entry: './src/main.ts',
    output: {
        filename: 'jitsi-cafe.js',
        path: path.resolve(__dirname, 'dist'),
    },
    resolve: {
        modules: [
            path.resolve(__dirname, 'node_modules'),
            path.resolve(__dirname, 'src')
        ],
        extensions: ['.ts', '.js']
    },
    module: {
        rules: [{
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: [/node_modules/],
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    }
};