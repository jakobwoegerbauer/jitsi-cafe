export class Vec2 {
    x: number;
    y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    toString() {
        return `${this.x} \t${this.y}`;
    }

    static zero() {
        return new Vec2(0, 0);
    }

    static diag(value: number) {
        return new Vec2(value, value);
    }

    static clone(v: Vec2): Vec2 {
        return new Vec2(v.x, v.y);
    }

    static add(a: Vec2, b: Vec2) {
        return new Vec2(a.x + b.x, a.y + b.y);
    }

    static subtract(a: Vec2, b: Vec2): Vec2 {
        return new Vec2(a.x - b.x, a.y - b.y);
    }

    static rotate(v: Vec2, angle: number): Vec2 {
        return new Vec2(
            Math.cos(angle) * v.x - Math.sin(angle) * v.y,
            Math.sin(angle) * v.x + Math.cos(angle) * v.y
        );
    }

    static magnitude(v: Vec2): number {
        return Math.sqrt(v.x * v.x + v.y * v.y);
    }

    static scale(a: Vec2, b: number) {
        return new Vec2(a.x * b, a.y * b);
    }

    static uniform(v: Vec2) {
        let magnitude = Vec2.magnitude(v);
        if (magnitude == 0) {
            return Vec2.zero();
        }
        return Vec2.scale(v, 1 / Vec2.magnitude(v));
    }

    static negate(v: Vec2) {
        return Vec2.scale(v, -1);
    }

    static max(a: Vec2, b: Vec2) {
        return new Vec2(Math.max(a.x, b.x), Math.max(a.y, b.y));
    }

    static min(a: Vec2, b: Vec2) {
        return new Vec2(Math.min(a.x, b.x), Math.min(a.y, b.y));
    }

    static scalarMult(a: Vec2, b: Vec2) {
        return new Vec2(a.x * b.x, a.y * b.y);
    }

    static scalarDiv(a: Vec2, b: Vec2) {
        return new Vec2(a.x / b.x, a.y / b.y);
    }

    static linearCombination(va: Vec2, a: number, vb: Vec2, b: number) {
        let v = Vec2.subtract(vb, va);
        return Vec2.add(va, Vec2.scale(v, b / (a + b)));
    }
}