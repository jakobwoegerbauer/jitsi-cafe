import { Vec2 } from "./vec2";

export class MapPosition extends Vec2 {
    
    rotation: number;

    constructor(x: number, y: number, rotation: number) {
        super(x, y);
        this.rotation = rotation;
    }

    static move(pos: MapPosition, vector: Vec2) {
        let newPos = Vec2.add(pos, vector);
        return new MapPosition(newPos.x, newPos.y, pos.rotation);
    }

    static clone(pos: MapPosition) {
        return new MapPosition(pos.x, pos.y, pos.rotation);
    }
}

export class ParticipantStatus extends MapPosition {
    areaId: string;

    constructor(x: number, y: number, rotation: number, areaId: string) {
        super(x, y, rotation);
        this.areaId = areaId;
    }

    static of(pos: Vec2, rotation: number, areaId: string) {
        return new ParticipantStatus(pos.x, pos.y, rotation, areaId);
    }

    static move(status: ParticipantStatus, target: MapPosition) {
        return new ParticipantStatus(target.x, target.y, target.rotation, status.areaId);
    }
}
