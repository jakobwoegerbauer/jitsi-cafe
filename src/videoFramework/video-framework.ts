import { Participant, RemoteParticipant } from "../participant";
import { OnlyNearbyVideos } from "./only-nearby-videos";
import { JitsiHandler } from "./jitsi-handler";
import { VideoHandler } from "./video-handler";
import { GridFramework } from "./grid-framework";

class VideoFramework {
    private handlers: Map<string, VideoHandler> = new Map();
    private activeHandler: VideoHandler;

    init() {
        let jitsiHandler = new JitsiHandler();
        this.handlers.set(jitsiHandler.getName(), jitsiHandler);
        /*let wcomHandler = new WeightedCenterVideoHandler();
        this.handlers.set(wcomHandler.getName(), wcomHandler);*/
        let gridHandler = new GridFramework();
        this.handlers.set(gridHandler.getName(), gridHandler);
        let nearbyHandler = new OnlyNearbyVideos();
        this.handlers.set(nearbyHandler.getName(), nearbyHandler);

        this.activeHandler = jitsiHandler;
    }

    getRegisteredHandlerNames() {
        return Array.from(this.handlers).map(v => v[0]);
    }

    getActiveHandlerName(): any {
        return this.activeHandler.getName();
    }

    switchVideoHandler(name: string) {
        let nextHandler = this.handlers.get(name);
        if (nextHandler) {
            this.activeHandler.deactivate();
            this.activeHandler = nextHandler;
            this.activeHandler.activate();
        }
    }

    onWindowResize() {
        if(this.activeHandler) {
            this.activeHandler.deactivate();
            this.activeHandler.activate();
        }
    }

    update() {
        this.activeHandler.update();
    }

    draw(ctx: CanvasRenderingContext2D) {
        this.activeHandler.draw(ctx);
    }

    updateLocalPosition() {
        this.activeHandler.updateLocalPosition();
    }

    updateRemotePosition(participant: RemoteParticipant) {
        this.activeHandler.updateRemotePosition(participant);
    }

    removeParticipant(participant: Participant) {
        this.activeHandler.removeParticipant(participant);
    }
}

export const videoFramework = new VideoFramework();