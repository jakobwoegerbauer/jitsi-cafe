import { Participant, RemoteParticipant } from "../participant";

export interface VideoHandler {
    getName(): string;
    activate(): void;
    deactivate(): void;
    onWindowResize(): void;
    update(): void;
    draw(ctx: CanvasRenderingContext2D): void;
    updateLocalPosition(): void;
    updateRemotePosition(participant: RemoteParticipant): void;
    removeParticipant(participant: Participant): void;
}