import { Box, BoxCollider } from "../box-collider";
import { Vec2 } from "../vec2";
import { jitsiCafe } from "../jitsi-cafe-room";
import { Participant } from "../participant";
import { settings } from "../settings";
import { UIState } from "./ui-state";
import { VideoHandler } from "./video-handler";

const filmStripStyle = `
    width: 100%;
    height: 100%;
    margin: 0;
`;
const remoteVideosContainerStyle = `
    display: block;
    width: 100%;
    height: 100%;
`;
const localVideoContainerStyle = `
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
`;

export class WeightedCenterVideoHandler implements VideoHandler {
    filmstrip: HTMLElement;
    remoteVideosContainer: HTMLElement;
    localVideoContainer: HTMLElement;
    collider = new BoxCollider<Participant>();

    videoDefaultStyle: string;
    localVideoDefaultStyle: string;

    
    getName() {
        return "weighted center of mass video handler";
    }

    activate() {
        /*
        APP.store.subscribe(() => {
            let tileViewEnabled = APP.store.getState()["features/video-layout"].tileViewEnabled;
            if (tileViewEnabled == undefined) {
                return;
            }
            if (tileViewEnabled != this.enabled) {
                this.enabled = tileViewEnabled;
                if (this.enabled) {
                    if (this.nextHandler) {
                        this._performHandlerSwitch();
                    } else {
                        this.activeHandler.activate();
                    }
                } else {
                    this.activeHandler.deactivate();
                }
            }
        });

        this.enabled = APP.store.getState()["features/video-layout"].tileViewEnabled;
*/

        this.filmstrip = document.getElementById("filmstripRemoteVideos");
        this.remoteVideosContainer = document.getElementById("filmstripRemoteVideosContainer");
        this.localVideoContainer = document.getElementById("localVideoTileViewContainer");
        uiState.saveState([this.filmstrip, this.remoteVideosContainer, this.localVideoContainer]);
        this.localVideoDefaultStyle = this.getVideoContainer(jitsiCafe.participantSelf.id)?.style?.cssText;
        this.videoDefaultStyle = this.getVideoContainer(jitsiCafe.participants.values().next().value.id)?.style?.cssText;
        this.onWindowResize();
    }

    deactivate() {
        uiState.restoreState([this.filmstrip, this.remoteVideosContainer, this.localVideoContainer]);
        this.collider.iterateBoxes(b => {
            this.getVideoContainer(b.key.id).style.cssText = b.key == jitsiCafe.participantSelf ? this.localVideoDefaultStyle : this.videoDefaultStyle;
        });
    }

    onWindowResize() {
        this.filmstrip.style.cssText = filmStripStyle;
        this.remoteVideosContainer.style.cssText = remoteVideosContainerStyle;
        this.localVideoContainer.style.cssText = localVideoContainerStyle;
        this.update(true);
    }

    updateLocalPosition() {
        // do nothing
    }

    updateRemotePosition(p: Participant) {
        // do nothing
    }

    removeParticipant(participant: Participant) {
        this.collider.removeBox(participant);
    }

    update(forceAll = false) {
        this._updatePositions();
        if(forceAll) {
            this.collider.update();
            this.collider.iterateBoxes(box => this._setVideoPosition(box));
        } else {
            this.collider.update().forEach(box => {
                this._setVideoPosition(box);
            });
        }        
    }

    draw(ctx: CanvasRenderingContext2D) {
        // do nothing
    }

    _updatePositions() {
        let self = jitsiCafe.participantSelf;
        let pos = this._getPosition(self, settings.videoSize);
        this.collider.setBox(self, pos, settings.videoSize, true);
        jitsiCafe.participants.forEach(p => {
            let pos = this._getPosition(p, settings.videoSize);
            this.collider.setBox(p, pos, settings.videoSize);
        });
    }

    private getVideoContainer(participantId: string) {
        let element = document.getElementById("participant_" + participantId);
        if (!element) {
            element = document.getElementById("localVideoContainer");
        }
        return element;
    }

    private _setVideoPosition(box: Box<Participant>) {
        if (!box) {
            console.log("Box is null!");
            return;
        }
        let container = this.getVideoContainer(box.key.id);
        if (container) {
            container.style.cssText = this._getCssText(box);
        }
    }

    private _getPosition(p: Participant, size: Vec2): Vec2 {
        let vec = Vec2.subtract(p.position, jitsiCafe.participantSelf.position);
        let vecRotated = Vec2.rotate(vec, -jitsiCafe.participantSelf.position.rotation);
        let center = new Vec2(window.innerWidth / 2, window.innerHeight / 2);

        return Vec2.subtract(Vec2.add(center, vecRotated), Vec2.scale(size, 0.5));
    }

    private _getCssText(box: Box<Participant>) {
        let pos = box.getTopLeft();
        return `
            position: absolute;
            left: ${pos.x};
            top: ${pos.y};
            width: ${box.size.x};
            height: ${box.size.y};
        `;
    }
}

const uiState = new UIState();