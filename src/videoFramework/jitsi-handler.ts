import { Participant, RemoteParticipant } from "../participant";
import { VideoHandler } from "./video-handler";

export class JitsiHandler implements VideoHandler {
    
    getName(): string {
        return "original Jitsi videos";
    }
    activate(): void {
        // do nothing
    }
    deactivate(): void {
        // do nothing
    }
    onWindowResize(): void {
        // do nothing
    }
    update(): void {
        // do nothing
    }
    draw(): void {
        // do nothing
    }
    updateLocalPosition(): void {
        // do nothing
    }
    updateRemotePosition(participant: RemoteParticipant): void {
        // do nothing
    }
    removeParticipant(participant: Participant) {
        // do nothing
    }

}