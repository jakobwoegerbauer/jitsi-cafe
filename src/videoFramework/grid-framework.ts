import { jitsiCafe } from "../jitsi-cafe-room";
import { Participant, RemoteParticipant } from "../participant";
import { settings } from "../settings";
import { Vec2 } from "../vec2";
import { VideoHandler } from "./video-handler";

export class GridFramework implements VideoHandler {

    private grid = new Grid();

    getName(): string {
        return "Local grid framework"
    }
    activate(): void {
        this.onWindowResize();
    }
    deactivate(): void {
        // do nothing
    }
    onWindowResize(): void {
        this.grid.setSize(new Vec2(window.innerWidth, window.innerHeight));
    }
    update(): void {
        // do nothing
    }
    draw(ctx: CanvasRenderingContext2D): void {
        let size = this.grid.getVideoSize();
        this.grid.getAllPositions().forEach(p => {
            let c = p.participant.getVideoTrack()?.containers;
            if (c && c.length > 0) {
                ctx.drawImage(c[0] as HTMLVideoElement, p.screenPos.x, p.screenPos.y, size.x, size.y);
            }
        });
    }
    updateLocalPosition(): void {
        this.grid.update(jitsiCafe.participantSelf);
        jitsiCafe.participants.forEach(p => this.grid.update(p));
    }
    updateRemotePosition(participant: RemoteParticipant): void {
        this.grid.update(participant);
    }
    removeParticipant(participant: Participant) {
        this.grid.removeParticipant(participant);
    }
}

class ScreenParticipant {
    participant: Participant;
    screenPos: Vec2;

    constructor(participant: Participant, screenPos: Vec2) {
        this.participant = participant;
        this.screenPos = screenPos;
    }
}

class Grid {
    private windowSize: Vec2 = Vec2.zero();
    private gridSize: Vec2 = Vec2.zero();
    private gridPositions: Map<number, Map<number, Vec2>> = new Map();
    private videoSize: Vec2 = Vec2.zero();

    private assignment: ScreenParticipant[] = [];
    private screenPositions: Map<Participant, Vec2> = new Map();

    private readonly outerMargin = new Vec2(50, 100);
    private readonly innerMargin = new Vec2(10, 10);

    setSize(size: Vec2) {
        this.windowSize = size;
        this.recalculateGridPositions();
    }

    update(participant: Participant) {
        if (this.shouldBeVisible(participant)) {
            if (!this.screenPositions.has(participant)) {
                this.screenPositions.set(participant, this.getScreenPosition(participant));
                this.recalculateGridSize();
            } else {
                this.screenPositions.set(participant, this.getScreenPosition(participant));
                this.recalculatAssignment();
            }
        } else {
            if (this.screenPositions.has(participant)) {
                this.screenPositions.delete(participant);
                this.recalculateGridSize();
            }
        }
    }

    removeParticipant(participant: Participant) {
        if (this.shouldBeVisible(participant)) {
            this.screenPositions.delete(participant);
            this.recalculateGridSize();
        } else {
            this.screenPositions.delete(participant);
        }
    }

    getAllPositions() {
        return this.assignment;
    }

    getVideoSize() {
        return this.videoSize;
    }

    private shouldBeVisible(participant: Participant) {
        return Vec2.magnitude(Vec2.subtract(participant.position, jitsiCafe.participantSelf.position)) <= settings.maxHearingDistance
        && participant.position.areaId == jitsiCafe.participantSelf.position.areaId
        && participant.getVideoTrack();
    }

    private recalculateGridSize() {
        this.gridSize.x = 1;
        this.gridSize.y = 1;
        while (this.gridSize.x * this.gridSize.y < this.screenPositions.size) {
            if (this.gridSize.x == this.gridSize.y)
                this.gridSize.x++;
            else
                this.gridSize.y++;
        }
        this.recalculateGridPositions();
        this.recalculatAssignment();
    }

    private recalculateGridPositions() {
        this.gridPositions.clear();
        let marginSum = Vec2.add(Vec2.scale(this.outerMargin, 2), Vec2.scalarMult(Vec2.subtract(this.gridSize, Vec2.diag(1)), this.innerMargin));
        this.videoSize = Vec2.scalarDiv(Vec2.subtract(this.windowSize, marginSum), this.gridSize);
        let ratio = this.videoSize.x / this.videoSize.y;
        if (ratio >= 16 / 9) {
            this.videoSize.x = this.videoSize.y * 16 / 9;
        } else {
            this.videoSize.y = this.videoSize.x * 9 / 16;
        }

        let actualOuterMargin = Vec2.scale(Vec2.subtract(this.windowSize,
            Vec2.add(Vec2.scalarMult(this.innerMargin, Vec2.subtract(this.gridSize, Vec2.diag(1))),
                Vec2.scalarMult(this.videoSize, this.gridSize))), 1 / 2);

        for (let x = 0; x < this.gridSize.x; x++) {
            let col: Map<number, Vec2> = new Map();
            for (let y = 0; y < this.gridSize.y; y++) {
                col.set(y, Vec2.add(actualOuterMargin, Vec2.scalarMult(Vec2.add(this.videoSize, this.innerMargin), new Vec2(x, y))));
            }
            this.gridPositions.set(x, col);
        }
    }

    private recalculatAssignment() {
        let participants: ScreenParticipant[] = [];
        this.screenPositions.forEach((_, p) => participants.push(new ScreenParticipant(p, this.screenPositions.get(p))));

        let positions: Vec2[] = [];
        for (let x = 0; x < this.gridSize.x; x++) {
            for (let y = 0; y < this.gridSize.y && positions.length < participants.length; y++) {
                positions.push(new Vec2(x, y));
            }
        }
        this.assignment = [];
        this.assignSubset(participants, positions, true);
    }

    private assignSubset(participants: ScreenParticipant[], positions: Vec2[], horizontal: boolean) {
        if (participants.length == 0) {
            return;
        }
        if (participants.length == 1) {
            let pos = positions[0];
            this.assignment.push(new ScreenParticipant(participants[0].participant, this.gridPositions.get(pos.x).get(pos.y)));
            return;
        }
        participants = participants.sort(Grid.orderParticipants(horizontal));
        positions = positions.sort(Grid.orderPositions(horizontal));
        let mid = Math.floor(participants.length / 2);
        this.assignSubset(participants.slice(0, mid), positions.slice(0, mid), !horizontal);
        this.assignSubset(participants.slice(mid), positions.slice(mid), !horizontal);
    }

    private getScreenPosition(p: Participant): Vec2 {
        let vec = Vec2.subtract(p.position, jitsiCafe.participantSelf.position);
        let vecRotated = Vec2.rotate(vec, -jitsiCafe.participantSelf.position.rotation);
        let center = Vec2.scale(this.windowSize, 1 / 2);
        return Vec2.add(center, Vec2.scale(vecRotated, settings.zoom));
    }

    private static orderParticipants(horizontal: boolean) {
        return (a: ScreenParticipant, b: ScreenParticipant) => horizontal ? a.screenPos.x - b.screenPos.x : a.screenPos.y - b.screenPos.y;
    }

    private static orderPositions(horizontal: boolean) {
        return (a: Vec2, b: Vec2) => {
            let diff = Vec2.subtract(a, b);
            if (horizontal) {
                return diff.x != 0 ? diff.x : diff.y;
            } else {
                return diff.y != 0 ? diff.y : diff.x;
            }
        }
    }

}