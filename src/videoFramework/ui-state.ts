export class UIState {
    private styles: Map<HTMLElement, string> = new Map();

    saveState(elements: HTMLElement[]) {
        elements.forEach(el => {
            this.styles.set(el, el?.style?.cssText);
        });
    }

    restoreState(elements: HTMLElement[]) {
        elements.forEach(el => {
            if(el) {
                el.style.cssText = this.styles.get(el);
            }
        });
    }
}
