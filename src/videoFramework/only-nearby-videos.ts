import { APP } from "../../jitsiClassDef/jitsi";
import { Vec2 } from "../vec2";
import { jitsiCafe } from "../jitsi-cafe-room";
import { Participant, RemoteParticipant } from "../participant";
import { settings } from "../settings";
import { VideoHandler } from "./video-handler";

export class OnlyNearbyVideos implements VideoHandler {

    static readonly NAME = "show only nearby videos";

    getName(): string {
        return OnlyNearbyVideos.NAME;
    }

    private _setVisible(p: RemoteParticipant, visible: boolean) {
        let el = document.getElementById("participant_" + p.id);
        if (el) {
            el.style.display = visible ? this._getVisibleDisplayValue() : "none";
        }
    }

    private _getVisibleDisplayValue() {
        return APP.store.getState()["features/video-layout"].tileViewEnabled ? "block" : "inline-block";
    }

    private _shouldBeVisible(p: RemoteParticipant) {
        let dist = Vec2.magnitude(Vec2.subtract(jitsiCafe.participantSelf.position, p.position));
        return p.position.areaId == jitsiCafe.participantSelf.position.areaId && dist <= settings.maxHearingDistance;
    }

    activate(): void {
        this.updateLocalPosition();
    }

    deactivate(): void {
        jitsiCafe.participants.forEach(p => this._setVisible(p, true));
    }

    onWindowResize(): void {

    }
    update(): void {

    }
    draw(): void {
        
    }
    updateLocalPosition(): void {
        jitsiCafe.participants.forEach(p => this._setVisible(p, this._shouldBeVisible(p)));
    }
    updateRemotePosition(participant: RemoteParticipant): void {
        this._setVisible(participant, this._shouldBeVisible(participant));
    }
    removeParticipant(participant: Participant) {
        // do nothing
    }
}