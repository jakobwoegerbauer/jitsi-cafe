import { jitsiCafe } from "./jitsi-cafe-room";
import { APP } from '../jitsiClassDef/jitsi';

function initjitsiCafe(conference: any) {
    if (APP && APP.store) {
        APP.store.subscribe(_ => {
            if (conference._room && !jitsiCafe.initializeStarted) {
                jitsiCafe.init(conference);
            }
        });
    } else {
        setTimeout(_ => { initjitsiCafe(conference); }, 100);
    }
}

initjitsiCafe(APP.conference);