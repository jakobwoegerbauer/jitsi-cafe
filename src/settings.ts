import { Vec2 } from "./vec2";

class Settings {
    askForNameOnInit = true;
    framesPerSecond = 30;
    updatesPerSecond = 30;
    sendsPerSecond = 20;
    circleRadius = 50;
    speed = 12.0;
    rotateSpeed = 3 * Math.PI / 180;
    canvasWidth = 1000;
    canvasHeight = 1000;
    zoom = 1;
    movementDirection: "absolute" | "view" = "absolute";
    movementDirectionOptions = ["absolute", "view"];
    keys = {
        UP: 'ArrowUp',
        DOWN: 'ArrowDown',
        LEFT: 'ArrowLeft',
        RIGHT: 'ArrowRight',
        ROTATE_LEFT: ',',
        ROTATE_RIGHT: '.'
    };
    colorSelf = "red";
    colorOthers = "blue";
    maxHearingDistance = 400;
    maxHearingDistanceColor: "grey";
    mapView: 'fullscreen' | 'window' = 'fullscreen';
    mapViewOptions = ['fullscreen', 'window'];
    backgroundColor: string = "transparent";
    backgroundColorOptions = ["transparent", "white", "green", "black"];
    stillstandFadeout = false;
    fadeoutSpeed = 0.005;
    viewSize = "600px";
    viewSizeOptions = ["200px", "400px", "600px", "800px", "1000px"];
    videoSize = new Vec2(200, 200);
}

export const settings = new Settings();