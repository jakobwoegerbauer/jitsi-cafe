import { settings } from "../settings";
import { html, render } from '../../node_modules/lit-html/lit-html';
import { Header, DRAG_ID } from "./header";
import { videoFramework } from "../videoFramework/video-framework";
import { MobileMoveView } from "./mobile-move-view";

export class UI {
    root: HTMLElement;
    cafeWindow: HTMLElement;
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    header: Header;
    mobileView: MobileMoveView;
    mapView = settings.mapView;

    private _renderCanvas = (ui: UI) => html`
            <canvas id="jitsiCafeMap" class="${ui.header.visible ? '' : 'hide'} ${ui.mapView == "fullscreen" ? "fullscreen" : ""}"></canvas>
        `;

    private _render = (ui: UI) => html`
            ${ui.mobileView.render(ui)}
            <div id="cafeWindow" class="windowDiv">
                ${ui.header.render(ui)}
                ${ui.mapView == 'window' ? ui._renderCanvas(ui) : ''}
            </div>
            ${ui.mapView == 'fullscreen' ? ui._renderCanvas(ui) : ''}
        `;

    init() {
        this.root = document.getElementById("jitsiCafeUI");

        if (this.root) {
            // already initialized
            return;
        }

        this.root = document.createElement("div");
        this.root.setAttribute("id", "jitsiCafeUI");
        document.body.appendChild(this.root);
        window.addEventListener("resize", this._onWindowResize.bind(this));

        this.mobileView = new MobileMoveView();
        this.header = new Header();
        this.update();

        this.cafeWindow = document.getElementById("cafeWindow");
        this.setViewMode(settings.mapView);

        dragElement(document.getElementById(DRAG_ID), this.cafeWindow);
    }

    private _onWindowResize() {
        this.canvas.width = this.mapView == "window" ? settings.canvasWidth : window.innerWidth;
        this.canvas.height = this.mapView == "window" ? settings.canvasHeight : window.innerHeight;
        videoFramework.onWindowResize();
    }

    setViewMode(viewMode: typeof settings.mapView) {
        this.mapView = settings.mapView = viewMode;
        this.cafeWindow.style.width = settings.mapView == "window" ? settings.viewSize : "auto";
        ui.update();
        this.canvas = document.getElementById("jitsiCafeMap") as HTMLCanvasElement;
        this.canvas.onclick = (ev: MouseEvent) => {
            this.header.settingsVisible = false;
            ui.update();
        }
        this.context = this.canvas.getContext("2d");
        this._onWindowResize();
    }

    update() {
        render(this._render(this), this.root);
    }

    clearCanvas() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        if (settings.backgroundColor) {
            this.context.fillStyle = settings.backgroundColor;
            this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
        }
    }

    dispose() {
        this.root.style.cssText = "display: none";
        document.body.removeChild(this.root);
    }
}

export const ui = new UI();

// canvas drag helper functions
function dragElement(srcElmnt: HTMLElement, elmnt: HTMLElement) {
    srcElmnt.onmousedown = dragMouseDown;
    var posStopX = 0;
    var posStopY = 0;
    var posStartX = 0;
    var posStartY = 0;


    function dragMouseDown(e: DragEvent) {
        e = e || window.event as DragEvent;
        //e.preventDefault();
        // get the mouse cursor position at startup:
        posStartX = e.clientX;
        posStartY = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e: DragEvent) {
        e = e || window.event as DragEvent;
        e.preventDefault();
        // calculate the new cursor position:
        posStopX = posStartX - e.clientX;
        posStopY = posStartY - e.clientY;
        posStartX = e.clientX;
        posStartY = e.clientY;
        // set the element's new position:
        elmnt.style.top = Math.min(window.innerHeight - srcElmnt.offsetHeight, Math.max(elmnt.offsetTop - posStopY, 0)) + "px";
        elmnt.style.left = Math.min(window.innerWidth - srcElmnt.offsetWidth, Math.max(elmnt.offsetLeft - posStopX, 0)) + "px";
    }

    function closeDragElement() {
        // stop moving when mouse button is released:
        document.onmouseup = null;
        document.onmousemove = null;
    }
}