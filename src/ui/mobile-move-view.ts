import { html } from '../../node_modules/lit-html/lit-html';
import { jitsiCafe } from '../jitsi-cafe-room';
import { settings } from '../settings';
import { Vec2 } from '../vec2';
import { UI } from './ui';

export class MobileMoveView {
    render = (ui: UI) => {
        if (!ui.header.visible || !ui.mobileView.visible) {
            ui.mobileView.canvas = null;
        }
        return ui.header.visible && ui.mobileView.visible ? html`
            <canvas id="mobileMove" class="mobileMove">
            </canvas>
            <canvas id="mobileZoom" class="mobileZoom">
            </canvas>
        ` : '';
    }

    visible = false;

    private touchStartPos = Vec2.zero();
    private touchCurrentPos = Vec2.zero();
    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;

    private touchStartPosZoom = Vec2.zero();
    private touchCurrentPosZoom = Vec2.zero();
    private canvasZoom: HTMLCanvasElement;
    private contextZoom: CanvasRenderingContext2D;
    private zoomStart = settings.zoom;

    init() {
        this.canvas = document.getElementById("mobileMove") as HTMLCanvasElement;
        this.canvasZoom = document.getElementById("mobileZoom") as HTMLCanvasElement;
        if (this.canvas) {
            this.canvas.addEventListener("touchstart", this.touchStart.bind(this));
            this.canvas.addEventListener("touchmove", this.touchMove.bind(this));
            this.canvas.addEventListener("touchend", this.touchEnd.bind(this));
            this.context = this.canvas.getContext("2d");
            this.canvas.width = this.canvas.offsetWidth;
            this.canvas.height = this.canvas.offsetHeight;

            this.canvasZoom.addEventListener("touchstart", this.touchStartZoom.bind(this));
            this.canvasZoom.addEventListener("touchmove", this.touchMoveZoom.bind(this));
            this.canvasZoom.addEventListener("touchend", this.touchEndZoom.bind(this));
            this.contextZoom = this.canvasZoom.getContext("2d");
            this.canvasZoom.width = this.canvasZoom.offsetWidth;
            this.canvasZoom.height = this.canvasZoom.offsetHeight;
        }
    }

    touchStart(e: TouchEvent) {
        this.touchStartPos = this.touchCurrentPos = new Vec2(e.changedTouches[0].pageX, e.changedTouches[0].pageY);
    }

    touchMove(e: TouchEvent) {
        this.touchCurrentPos = new Vec2(e.changedTouches[0].pageX, e.changedTouches[0].pageY);
    }

    touchEnd(e: TouchEvent) {
        this.touchStartPos = this.touchCurrentPos;
    }

    touchStartZoom(e: TouchEvent) {
        this.touchStartPosZoom = this.touchCurrentPosZoom = new Vec2(e.changedTouches[0].pageX, e.changedTouches[0].pageY);
        this.zoomStart = settings.zoom;
    }

    touchMoveZoom(e: TouchEvent) {
        this.touchCurrentPosZoom = new Vec2(e.changedTouches[0].pageX, e.changedTouches[0].pageY);
    }

    touchEndZoom(e: TouchEvent) {
        this.touchStartPosZoom = this.touchCurrentPosZoom;
    }

    update() {
        if (!this.visible) {
            return;
        }
        if (!this.canvas) {
            this.init();
            return;
        }
        let v = Vec2.subtract(this.touchCurrentPos, this.touchStartPos);
        let magnitude = Vec2.magnitude(v);
        if (magnitude > 5) {
            let s = magnitude / 4;
            let moveVec = Vec2.rotate(Vec2.scale(Vec2.uniform(v), Math.min(s, settings.speed)), jitsiCafe.participantSelf.position.rotation);
            jitsiCafe.participantSelf.move(moveVec);
        }

        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.beginPath();
        this.context.arc(this.canvas.width / 2 + v.x, this.canvas.height / 2 + v.y, 20, 0, Math.PI * 2);
        this.context.fillStyle = "white";
        this.context.fill();

        let z = Vec2.subtract(this.touchCurrentPosZoom, this.touchStartPosZoom).y;
        if (Math.abs(z) > 5) {
            jitsiCafe.setZoom(this.zoomStart + z / 200);
        }

        this.contextZoom.clearRect(0, 0, this.canvasZoom.width, this.canvasZoom.height);
        this.contextZoom.fillStyle = "black";
        this.contextZoom.fillRect(0, this.canvasZoom.height / 2 + z - 2, this.canvasZoom.width, 4);
    }
}