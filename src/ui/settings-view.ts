import { html } from '../../node_modules/lit-html/lit-html';
import { audioFramework, availableAudioFrameworks, switchAudioFramework } from '../audio';
import { jitsiCafe } from '../jitsi-cafe-room';
import { settings } from '../settings';
import { videoFramework } from '../videoFramework/video-framework';
import { ui, UI } from './ui';

function selectTag<T>(label: string, onchange: (e: Event) => void, options: T[], selected: T, itemMapper: (item: T) => any = i => i) {
    return html`
    <td>
        <label>${label}</label>
    </td>
    <td>
        <select @change=${onchange}>
            ${options.map(item => item == selected ?
        html`<option selected>${itemMapper(item)}</option>`
        : html`<option>${itemMapper(item)}</option>`)}
        </select>
    </td>`;
}

function checkboxTag(label: string, onchange: (e: Event) => void, checked: boolean) {
    return html`
        <td><label>${label}</label></td>
        ${checked ?
            html`<td><input type="checkbox" checked @change=${onchange}></input></td>`
            : html`<td><input type="checkbox" @change=${onchange}></input></td>`
        }`;
}

export class SettingsView {
    render = (ui: UI) => ui.header.settingsVisible ? html`
        <div class="settings">
            <div class="ib">
                <table>
                    <tr>
                        ${selectTag("Background color", this.backgroundChanged, settings.backgroundColorOptions, settings.backgroundColor)}
                    </tr>
                    <tr>
                        ${selectTag("View mode", this.viewModeChanged, settings.mapViewOptions, settings.mapView)}                
                    </tr>
                    ${ui.mapView == "window" ?
            html`
                            <tr>
                            ${selectTag("View size", this.sizeChanged, settings.viewSizeOptions, settings.viewSize)}
                            </tr>
                        ` : ''
        }
                    <tr>
                        ${selectTag("Audio Framework", this.audioFrameworkChanged, availableAudioFrameworks, audioFramework, i => i.getName())}
                    </tr>
                    <tr>
                        ${selectTag("Video Framework", this.videoFrameworkChanged, videoFramework.getRegisteredHandlerNames(), videoFramework.getActiveHandlerName())}
                    </tr>
                    <tr>
                        ${selectTag("Movement", this.movementChanged, settings.movementDirectionOptions, settings.movementDirection)}                
                    </tr>
                    <tr>
                        ${checkboxTag("Joystick for mobile devices", this.mobileJoystickChanged, ui.mobileView.visible)}
                    </tr>
                    <tr>
                        ${checkboxTag("Stillstand fadeout", this.fadeoutChanged, settings.stillstandFadeout)}
                    </tr>
                </table>
                <br/>
                <a href="https://gitlab.com/jakobwoegerbauer/jitsi-cafe" target="_blank">Project homepage: https://gitlab.com/jakobwoegerbauer/jitsi-cafe</a>
            </div>
        </div>
        ` : '';

    backgroundChanged(e: Event) {
        (e.target as HTMLElement).blur();
        settings.backgroundColor = (e.target as HTMLSelectElement).value;
        ui.canvas.style.background = (e.target as HTMLSelectElement).value;
    }

    viewModeChanged(e: Event) {
        (e.target as HTMLElement).blur();
        ui.setViewMode((e.target as HTMLSelectElement).value as typeof settings.mapView);
    }

    sizeChanged(e: Event) {
        (e.target as HTMLElement).blur();
        settings.viewSize = (e.target as HTMLSelectElement).value;
        ui.cafeWindow.setAttribute("style", "width: " + settings.viewSize);
    }

    audioFrameworkChanged(e: Event) {
        (e.target as HTMLElement).blur();
        switchAudioFramework((e.target as HTMLSelectElement).value);
    }

    videoFrameworkChanged(e: Event) {
        (e.target as HTMLElement).blur();
        videoFramework.switchVideoHandler((e.target as HTMLSelectElement).value);
    }

    movementChanged(e: Event) {
        (e.target as HTMLElement).blur();
        settings.movementDirection = (e.target as HTMLSelectElement).value as typeof settings.movementDirection;
    }

    mobileJoystickChanged(e: Event) {
        (e.target as HTMLElement).blur();
        ui.mobileView.visible = (e.target as HTMLInputElement).checked;
        ui.update();
    }

    fadeoutChanged(e: Event) {
        (e.target as HTMLElement).blur();
        settings.stillstandFadeout = (e.target as HTMLInputElement).checked;
        if(!settings.stillstandFadeout) {
            jitsiCafe.mapAlpha = 1;
            jitsiCafe.participantSelf.alpha = 1;
            jitsiCafe.participants.forEach(p => p.alpha = 1);
        }
    }
}