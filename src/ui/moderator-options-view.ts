import { html, render } from '../../node_modules/lit-html/lit-html';
import { jitsiCafe } from '../jitsi-cafe-room';
import { ui, UI } from './ui';

export class ModeratorOptionsView {
    render = (ui: UI) => ui.header.moderatorOptionsVisible ? html`
        <div class="settings">
            <div class="ib">
                <button class="jitsiCafeMenuButton button-control" @click=${this.toggleSpeakerMode}>${jitsiCafe.participantSelf.isSpeaker ? 'disable speaker mode' : 'enable speaker mode' }</button>
            </div>
        </div>
        ` : '';

    toggleSpeakerMode() {
        jitsiCafe.participantSelf.setSpeaker(!jitsiCafe.participantSelf.isSpeaker);
        jitsiCafe.publishPosition();
        ui.update();
    }
}