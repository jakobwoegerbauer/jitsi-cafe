import './header.css';
import { html } from '../../node_modules/lit-html/lit-html';
import { jitsiCafe } from '../jitsi-cafe-room';
import { ModeratorOptionsView } from './moderator-options-view';
import { SettingsView } from './settings-view';
import { UI, ui } from './ui';
import { RoomView } from './room-view';

export const DRAG_ID = "dragAnchor";

export class Header {
    settingsView: SettingsView = new SettingsView();
    roomView = new RoomView();
    moderatorOptionsView: ModeratorOptionsView = new ModeratorOptionsView();
    settingsVisible: boolean = false;
    moderatorOptionsVisible: boolean = false;
    visible: boolean = true;

    render(ui: UI){
        return html`
        <div id="jitsiCafeHeader">
            <a href="https://gitlab.com/jakobwoegerbauer/jitsi-cafe/-/blob/master/README.md" target="_blank" class="menuItem tutorialLink">Jitsi-Café</a>
            <button @click=${ui.header.showClicked} class="menuItem jitsiCafeMenuButton button-control">${ui.header.visible ? 'Hide' : 'Show'}</button>
            ${jitsiCafe.isModerator() ? html`<button @click=${ui.header.moderatorOptionsClicked} class="menuItem jitsiCafeMenuButton button-control">Moderator Options</button>` : ''}
            <button id="btnSettings" @click=${ui.header.settingsClicked} class="menuItem jitsiCafeMenuButton button-control">Settings</button>
            <a href="https://gitlab.com/jakobwoegerbauer/jitsi-cafe/-/blob/master/README.md#controls" target="_blank" class="menuItem tutorialLink">Tutorial</a>
            ${ui.header.roomView.isAvailable ? html`<button @click=${ui.header.roomClicked} class="menuItem jitsiCafeMenuButton button-control">${ui.header.roomView.roomData.roomName}</button>` : ''}
            <div id="${DRAG_ID}" class="drag"></div>
            ${this.settingsView.render(ui)}
            ${this.roomView.render(ui)}
            ${this.moderatorOptionsView.render(ui)}
        </div>
        `;
    }

    private hideAll() {
        ui.header.settingsVisible = false;
        ui.header.moderatorOptionsVisible = false;
        ui.header.roomView.isVisible = false;
    }

    settingsClicked(e: MouseEvent) {
        let v = !ui.header.settingsVisible;
        ui.header.hideAll();
        ui.header.settingsVisible = v;        
        ui.update();
    }

    moderatorOptionsClicked(e: MouseEvent) {
        let v = !ui.header.moderatorOptionsVisible;
        ui.header.hideAll();
        ui.header.moderatorOptionsVisible = v;      
        ui.update();
    }

    showClicked(e: MouseEvent) {
        let v = !ui.header.visible;
        ui.header.hideAll();
        ui.header.visible = v;
        ui.update();
    }

    roomClicked(e: MouseEvent) {
        let v = !ui.header.roomView.isVisible;
        ui.header.hideAll();
        ui.header.roomView.isVisible = v;
        ui.update();
    }
}