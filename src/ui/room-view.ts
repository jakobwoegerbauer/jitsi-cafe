import { html } from '../../node_modules/lit-html/lit-html';
import { RoomDataMessage } from '../communication';
import { jitsiCafe } from '../jitsi-cafe-room';
import { roomData, RoomData } from '../maps/rooms';
import { ui, UI } from './ui';

export class RoomView {
    isAvailable = false;
    isVisible = false;
    roomData: RoomData = new RoomData("", "", "");

    render = (ui: UI) => ui.header.roomView.isAvailable && ui.header.roomView.isVisible ? html`
        <div class="settings">
            ${ui.header.roomView.roomData.gameName ? html`
                <p>
                    Current game: ${ui.header.roomView.roomData.gameName}
                </p>
                ${ui.header.roomView.roomData.gameLink ? html`
                    <p>    
                        Link: <a href="${ui.header.roomView.roomData.gameLink}" target="_blank">${ui.header.roomView.roomData.gameLink}</a>
                    </p>
                ` : ''}    
            ` : ''}
            <button style="background-color: black" @click=${ui.header.roomView.updateClick.bind(ui.header.roomView)}>Set current game</button>
        </div>
        ` : '';

    setRoom(roomData: RoomData) {
        ui.header.roomView.roomData = roomData;
        ui.header.roomView.isAvailable = true;
        ui.header.roomView.isVisible = true;
        ui.update();
    }

    update() {
        ui.header.roomView.roomData = roomData.get(ui.header.roomView.roomData.roomName) ?? new RoomData("", "", "");
        if (ui.header.roomView.isAvailable) {
            ui.update();
        }
    }

    disable() {
        ui.header.roomView.isAvailable = false;
        ui.header.roomView.isVisible = false;
        ui.update();
    }

    private updateClick(e: MouseEvent) {
        let name = prompt("Please enter the name of the game:");
        let rd = roomData.get(ui.header.roomView.roomData.roomName) ?? new RoomData("", "", "");
        if (rd && (name || name == "")) {
            rd.gameName = name;
            let link = "";
            if (name != "") {
                link = prompt("Please enter the link for participating:", "https://");
                if (link && link != "https://") {
                    rd.gameLink = link;
                }
            }
            jitsiCafe.communication.send(new RoomDataMessage(rd), 10);
            ui.header.roomView.setRoom(rd);
        }
    }
}