import { Vec2 } from "./vec2";
import { settings } from "./settings";

export class BoxCollider<T> {

    private boxes: Map<T, Box<T>> = new Map();
    private centerBox: Box<T>;

    setBox(key: T, pos: Vec2, size: Vec2, fixed: boolean = false) {
        let b = this.boxes.get(key);
        let center = Vec2.add(pos, Vec2.scale(size, 0.5));
        if (!b) {
            b = new Box(key, center, center, size, fixed);
            this.boxes.set(key, b);
        } else {
            b.targetCenter = center;
            b.size = size;
            b.fixed = fixed;
        }
        if (fixed) {
            this.centerBox = b;
        }
    }

    update(): Box<T>[] {
        let boxes: Box<T>[] = [];
        let weightedCenterOfMass = Vec2.zero();
        let centerOfMass = this.centerBox.targetCenter;
        let comWeight = 0;
        boxes.push(this.centerBox);
        this.iterateBoxes(b => {
            b.oldCenter = b.center;
            b.center = b.targetCenter;
            if (b != this.centerBox && Vec2.magnitude(Vec2.subtract(b.targetCenter, this.centerBox.targetCenter)) <= settings.maxHearingDistance) {
                let wVector = Vec2.magnitude(Vec2.subtract(b.targetCenter, this.centerBox.targetCenter));
                let w = 1/Math.sqrt(wVector);
                comWeight += w;
                weightedCenterOfMass = Vec2.add(weightedCenterOfMass, Vec2.scale(b.targetCenter, w));
                centerOfMass = Vec2.add(centerOfMass, b.targetCenter);
                boxes.push(b);
            }
        });

        centerOfMass = Vec2.scale(centerOfMass, 1/boxes.length)
        weightedCenterOfMass = Vec2.scale(weightedCenterOfMass, 1/comWeight);

        boxes.sort((a, b) => {
            var distA = Vec2.magnitude(Vec2.subtract(a.targetCenter, centerOfMass));
            var distB = Vec2.magnitude(Vec2.subtract(b.targetCenter, centerOfMass));
            if (distA == distB) {
                if (a == this.centerBox) return -1;
                return 1;
            }
            return distA - distB;
        });

        let done: Box<T>[] = [];
        for (let b of boxes) {
            done.push(b);
            b.center = b.targetCenter;
            let collisions = this.getCollisions(b, boxes);
            if (collisions.length == 0) {
                continue;
            }
            let v = Vec2.subtract(b.center, weightedCenterOfMass);
            if (Vec2.magnitude(v) < 1) {
                if (b == this.centerBox) {
                    v = new Vec2(0, 1);
                } else {
                    v = new Vec2(0, -1);
                }
            } else {
                v = Vec2.uniform(v);
            }

            let scale = 500;
            while (true) {
                let posLast = b.center;
                let vScaled = Vec2.scale(v, scale);
                b.center = Vec2.add(b.center, vScaled);
                collisions = this.getCollisions(b, done);

                if(collisions.length == 0) {
                    b.center = posLast;
                } 
                scale /= 2;
                if (scale < 1) {
                    break;
                }
            }
        }
        return Array.from(this.boxes.values()).filter(b => Vec2.magnitude(Vec2.subtract(b.center, b.oldCenter)));
    }

    getBox(key: T) {
        return this.boxes.get(key).clone();
    }

    removeBox(key: T) {
        this.boxes.delete(key);
    }

    private getCollisions(box: Box<T>, boxes: Box<T>[]) {
        let collidingBoxes: Box<T>[] = [];

        boxes.forEach(b => {
            if (b != box && box.intersects(b)) {
                if (!collidingBoxes.find(x => b == x)) {
                    collidingBoxes.push(b);
                }
            }
        });

        if (collidingBoxes.length >= 1) {
            collidingBoxes.push(box);
        }
        return collidingBoxes;
    }

    iterateBoxes(fn: (b: Box<T>) => void) {
        this.boxes.forEach((b, _) => {
            fn(b);
        })
    }
}

export class Box<T> {
    center: Vec2;
    targetCenter: Vec2;
    size: Vec2;
    fixed: boolean;
    key: T;
    oldCenter = Vec2.zero();

    constructor(key: T, center: Vec2, targetCenter: Vec2, size: Vec2, fixed: boolean = false) {
        this.key = key;
        this.center = center
        this.targetCenter = targetCenter;
        this.size = size;
        this.fixed = fixed;
    }

    getTopLeft() {
        return Vec2.add(this.center, Vec2.scale(this.size, -0.5));
    }

    getBottomRight() {
        return Vec2.add(this.center, Vec2.scale(this.size, 0.5));
    }

    intersects(other: Box<T>) {
        let p = this.getTopLeft();
        let op = other.getTopLeft();
        if (p.y >= op.y + other.size.y) return false;
        if (p.y + this.size.y <= op.y) return false;
        if (p.x >= op.x + other.size.x) return false;
        if (p.x + this.size.x <= op.x) return false;
        return true;
    }

    clone() {
        return new Box<T>(this.key, Vec2.clone(this.center), Vec2.clone(this.targetCenter), Vec2.clone(this.size), this.fixed);
    }

    toString() {
        return `${(this.key as any).id} \tpos: ${this.center} \tsize: ${this.size}`;
    }

    static getMass<T>(b: Box<T>) {
        return b.size.x * b.size.y;
    }
}