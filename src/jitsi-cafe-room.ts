import './jitsi-cafe.css';
import { LocalParticipant, Participant, RemoteParticipant } from "./participant";
import { APP, JitsiConference, JitsiParticipant } from "../jitsiClassDef/jitsi";
import { ui } from "./ui/ui";
import { Communication, Message, MessageType, StatusMessage, RequestInitDataMessage, RoomDataMessage } from "./communication";
import { settings } from "./settings";
import { audioFramework, availableAudioFrameworks } from "./audio";
import { Vec2 } from "./vec2";
//import { SimpleMap } from "./maps/simple-map";
import { videoFramework } from './videoFramework/video-framework';
import { OnlyNearbyVideos } from './videoFramework/only-nearby-videos';
import { createRoomMap, roomData } from './maps/rooms';
import { CafeMap } from './maps/cafe-map';
import { EventType, inputHandler } from './input-handler';

class JitsiCafeRoom {
    initializeStarted = false;
    conference: JitsiConference;
    participants: Map<String, RemoteParticipant>;
    participantSelf: LocalParticipant;
    allParticipants: Participant[];
    drawInterval: NodeJS.Timeout;
    updateInterval: NodeJS.Timeout;
    sendInterval: NodeJS.Timeout;
    lastPositionPublish: number = 0;
    communication: Communication;
    map: CafeMap = createRoomMap();
    changedSinceLastSend = false;
    mapAlpha = 1;

    constructor() {
        this.participants = new Map();
        this.allParticipants = [];
    }

    init(conference: JitsiConference) {
        if (this.initializeStarted) {
            return;
        }
        this.initializeStarted = true;
        if (settings.askForNameOnInit && !APP.conference.getLocalDisplayName()) {
            var name = prompt("Please enter your name", "");
            APP.conference._room.setDisplayName(name);
            APP.store.getState()["features/base/participants"][0].name = name;
        }

        console.debug("init Jitsi Café");
        this.conference = conference;
        this.communication = new Communication(conference);
        ui.init();
        this.setupMsgHandlers();
        inputHandler.init();
        inputHandler.addListener(EventType.KeyDown, this.keyDown.bind(this));

        conference.addConferenceListener('conference.userJoined', this._onUserJoined);
        conference.addConferenceListener('conference.userLeft', this._onUserLeft);

        this.participantSelf = new LocalParticipant(
            settings.colorSelf,
            conference.getMyUserId());
        this.participantSelf.position = this.map.getSpawn(this.participantSelf);
        this.allParticipants.push(this.participantSelf);

        for (var p of conference._room.getParticipants()) {
            this.onUserJoined(p.getId(), p);
        }

        this.drawInterval = setInterval(this.draw.bind(this), 1000 / settings.framesPerSecond);
        this.updateInterval = setInterval(this.update.bind(this), 1000 / settings.updatesPerSecond);
        this.sendInterval = setInterval(this.publishPosition.bind(this), 1000 / settings.sendsPerSecond);
        this.requestInitData();
        videoFramework.init();
        videoFramework.switchVideoHandler(OnlyNearbyVideos.NAME);
    }

    _onUserJoined(id: string, participant: JitsiParticipant) { jitsiCafe.onUserJoined(id, participant); }
    _onUserLeft(id: string, participant: JitsiParticipant) { jitsiCafe.onUserLeft(id, participant); }

    private keyDown(e: KeyboardEvent) {
        if (e.key == '+') {
            this.setZoom(settings.zoom + 0.1);
        } else if (e.key == '-') {
            this.setZoom(settings.zoom - 0.1);
        }
    }

    setZoom(zoom: number) {
        settings.zoom = Math.max(Math.min(zoom, 1.4), 0.3);
    }

    isModerator() {
        return APP.store.getState()["features/base/settings"].email == "ibinmoderator";
    }

    onUserJoined(id: string, participant: JitsiParticipant) {
        let p = new RemoteParticipant(
            settings.colorOthers,
            id,
            participant
        );
        p.position = this.map.getSpawn(p);
        this.participants.set(id, p);
        this.allParticipants.push(p);
        audioFramework.addParticipant(p);
    }

    onUserLeft(id: string, participant: JitsiParticipant) {
        let p = this.participants.get(id);
        audioFramework.removeParticipant(p);
        videoFramework.removeParticipant(p);
        this.allParticipants = this.allParticipants.filter(x => x != p);
        this.participants.delete(id);
    }

    setupMsgHandlers() {
        this.communication.setHandler(MessageType.STATUS, (sender: JitsiParticipant, message: Message) => {
            var msg = message as StatusMessage;
            var p = this.participants.get(sender.getId());
            p.isSpeaker = msg.isSpeaker;
            p.updatePosition(msg.position);
        });
        this.communication.setHandler(MessageType.REQUEST_INIT_DATA, (sender: JitsiParticipant, msg: Message) => {
            this.changedSinceLastSend = true;
            this.publishPosition();
            if(APP.conference._room.isModerator()) {
                roomData.forEach(r => {
                    this.communication.sendTo(sender.getId(), new RoomDataMessage(r), 10);
                });
            }
        });
        this.communication.setHandler(MessageType.ROOM_DATA, (sender: JitsiParticipant, msg: Message) => {
            let message = (msg as RoomDataMessage);
            roomData.set(message.roomData.roomName, message.roomData);
            ui.header.roomView.update();
        });
    }

    publishPosition(retries = 0, callback: () => void = null) {
        if (this.changedSinceLastSend || Date.now() - this.lastPositionPublish > 1000) {
            this.communication.send(new StatusMessage(
                this.participantSelf.position,
                this.participantSelf.isSpeaker
            ), retries, callback);
            this.changedSinceLastSend = false;
            this.lastPositionPublish = Date.now();
        }
    }

    private requestInitData() {
        this.communication.send(new RequestInitDataMessage(), 50);
    }

    update() {
        let up = Vec2.rotate(new Vec2(0, -1), this.participantSelf.position.rotation);
        let direction = Vec2.zero();
        let angle = 0;

        if (inputHandler.isKeyPressed(settings.keys.UP)) { direction = Vec2.add(direction, up); }
        if (inputHandler.isKeyPressed(settings.keys.DOWN)) { direction = Vec2.add(direction, Vec2.rotate(up, Math.PI)); }

        if (settings.movementDirection == "absolute") {
            if (inputHandler.isKeyPressed(settings.keys.LEFT)) { direction = Vec2.add(direction, Vec2.rotate(up, Math.PI * 3 / 2)); }
            if (inputHandler.isKeyPressed(settings.keys.RIGHT)) { direction = Vec2.add(direction, Vec2.rotate(up, Math.PI / 2)); }
            if (inputHandler.isKeyPressed(settings.keys.ROTATE_LEFT)) { angle -= 1; }
            if (inputHandler.isKeyPressed(settings.keys.ROTATE_RIGHT)) { angle += 1; }
        } else if (settings.movementDirection == "view") {
            if (inputHandler.isKeyPressed(settings.keys.LEFT)) { angle -= 1; }
            if (inputHandler.isKeyPressed(settings.keys.RIGHT)) { angle += 1; }
        }

        if (settings.stillstandFadeout) {
            this.updateAlpha();
        }

        if (Vec2.magnitude(direction) != 0) {
            this.participantSelf.move(Vec2.scale(direction, settings.speed));
            this.changedSinceLastSend = true;
            this.mapAlpha = 1;
        }
        if (angle != 0) {
            this.participantSelf.rotate(angle * settings.rotateSpeed);
            this.mapAlpha = 1;
        }

        ui.mobileView.update();
        this.map.update();
        videoFramework.update();
    }

    private updateAlpha() {
        this.mapAlpha = Math.max(this.mapAlpha - settings.fadeoutSpeed, 0);
        this.allParticipants.forEach(p => p.alpha = Math.max(p.alpha - settings.fadeoutSpeed, 0));
    }

    draw() {
        ui.clearCanvas();
        ui.context.save();
        videoFramework.draw(ui.context);
        ui.context.translate(ui.canvas.width / 2, ui.canvas.height / 2);
        this._drawCentered(ui.context);
        ui.context.restore();
    }

    private _drawCentered(ctx: CanvasRenderingContext2D) {
        let pos = this.participantSelf.position;
        ctx.save();
        ctx.scale(settings.zoom, settings.zoom);
        ctx.rotate(-pos.rotation);
        ctx.translate(-pos.x, -pos.y);
        ctx.globalAlpha *= this.mapAlpha;
        this.map.draw(ctx);
        ctx.restore();

        this.participants.forEach(p => {
            if (this.participantSelf.position.areaId == p.position.areaId) {
                ctx.save();
                ctx.rotate(-pos.rotation);
                ctx.scale(settings.zoom, settings.zoom);
                ctx.translate((p.position.x - pos.x), (p.position.y - pos.y));
                ctx.rotate(pos.rotation);
                p.draw(ctx);
                ctx.restore();
            }
        });
        ctx.save();
        ctx.scale(settings.zoom, settings.zoom);
        this.participantSelf.draw(ctx);
        ctx.restore();
    }
}

export const jitsiCafe = new JitsiCafeRoom();

(window as any).d = {
    jitsiCafe: jitsiCafe,
    ui: ui,
    videoFramework: videoFramework,
    audioFramework: audioFramework,
    roomData: roomData,
    settings: settings,
    availableAudioFrameworks: availableAudioFrameworks
};