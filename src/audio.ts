import { getContainer, JitsiConference } from "../jitsiClassDef/jitsi";
import { Vec2 } from "./vec2";
import { jitsiCafe } from "./jitsi-cafe-room";
import { RemoteParticipant } from "./participant";
import { settings } from "./settings";

export abstract class AudioFramework {
    abstract addParticipant(p: RemoteParticipant): void;
    abstract removeParticipant(p: RemoteParticipant): void;
    abstract updateRemotePosition(p: RemoteParticipant): void;
    abstract updateLocalPosition(): void;
    abstract getName(): string;
}

class Volume extends AudioFramework {
    getName() {
        return "distance";
    }
    addParticipant(p: RemoteParticipant) {
        this.updateRemotePosition(p);
    }
    removeParticipant(p: RemoteParticipant) {
        // nothing to do here
    }
    updateRemotePosition(p: RemoteParticipant) {
        if(p.position.areaId == jitsiCafe.participantSelf.position.areaId) {
            this.setVolume(p, p.isSpeaker ? 1 : getVolume(getDistance(p.position, jitsiCafe.participantSelf.position)))
        } else {
            this.setVolume(p, 0);
        }        
    }
    updateLocalPosition() {
        jitsiCafe.participants.forEach(p => this.updateRemotePosition(p));
    }

    private setVolume(p: RemoteParticipant, volume: number) {
        for (var t of p.jitsiParticipant.getTracksByMediaType("audio")) {
            for (var c of t.containers) {
                c.volume = volume;
            }
        }
    };
}

const FORWARD = new Vec2(0, -1);

class SpatialAudio extends AudioFramework {
    audioCtx = new AudioContext();
    panners: Map<RemoteParticipant, PannerNode> = new Map();

    constructor() {
        super();
        const listener = this.audioCtx.listener;
        const t = this.audioCtx.currentTime;

        this._setListenerDirection(FORWARD);
    }

    private _setListenerDirection(direction: Vec2) {
        const listener = this.audioCtx.listener;
        const t = this.audioCtx.currentTime;
        if (listener.forwardX) {
            listener.forwardX.setValueAtTime(direction.x, t);
            listener.forwardY.setValueAtTime(0, t);
            listener.forwardZ.setValueAtTime(direction.y, t);
            listener.upX.setValueAtTime(0, t);
            listener.upY.setValueAtTime(1, t);
            listener.upZ.setValueAtTime(0, t);
        } else {
            listener.setOrientation(direction.x, 0, direction.y, 0, 1, 0);
        }
    }
    
    getName() {
        return "spatial";
    }

    addParticipant(p: RemoteParticipant) {
        let track = p.getAudioTrack();
        if (!track) {
            setTimeout(() => this.addParticipant(p), 50);
            return;
        }
        let container = getContainer(track);
        let srcNode = this.audioCtx.createMediaStreamSource(container.srcObject as MediaStream);

        let panner = this.createPanner();
        this.panners.set(p, panner);

        srcNode.connect(panner);
        panner.connect(this.audioCtx.destination);
        container.pause();

        this.updateRemotePosition(p);
    }

    removeParticipant(p: RemoteParticipant) {
        let panner = this.panners.get(p);
        panner.disconnect();        
        getContainer(p.getAudioTrack())?.play();
        this.panners.delete(p);
    }

    private createPanner() {
        var panner = this.audioCtx.createPanner();
        panner.panningModel = 'HRTF';
        panner.distanceModel = 'linear';
        panner.refDistance = settings.speed;
        panner.maxDistance = settings.maxHearingDistance;
        panner.rolloffFactor = 1;
        panner.coneInnerAngle = 360;
        panner.coneOuterAngle = 0;
        panner.coneOuterGain = 0;

        if (panner.orientationX) {
            panner.orientationX.setValueAtTime(0, this.audioCtx.currentTime);
            panner.orientationY.setValueAtTime(1, this.audioCtx.currentTime);
            panner.orientationZ.setValueAtTime(0, this.audioCtx.currentTime);
        } else {
            panner.setOrientation(0, 1, 0);
        }
        return panner;
    }

    private _setPannerPosition(panner: PannerNode, pos: Vec2, height: number = 0) {
        const t = this.audioCtx.currentTime;

        if (panner.positionX) {
            panner.positionX.setValueAtTime(pos.x, t);
            panner.positionY.setValueAtTime(height, t);
            panner.positionZ.setValueAtTime(pos.y, t);
        } else {
            panner.setPosition(pos.x, height, pos.y);
        }
    }

    updateRemotePosition(p: RemoteParticipant) {
        const panner = this.panners.get(p);
        if (!panner){
            return;
        }
        let pos = Vec2.clone(p.position);
        let height = 0;
        if (p.position.areaId != jitsiCafe.participantSelf.position.areaId) {
            height = settings.maxHearingDistance + 100;
        }
        if (p.isSpeaker) {
            pos = Vec2.clone(jitsiCafe.participantSelf.position);
        }
        this._setPannerPosition(panner, pos, height);
    }

    updateLocalPosition() {
        const listener = this.audioCtx.listener;
        const t = this.audioCtx.currentTime;
        const pos = jitsiCafe.participantSelf.position;

        this._setListenerDirection(Vec2.rotate(FORWARD, pos.rotation));

        if (listener.positionX) {
            listener.positionX.setValueAtTime(pos.x, t);
            listener.positionY.setValueAtTime(0, t);
            listener.positionZ.setValueAtTime(pos.y, t);
        } else {
            listener.setPosition(pos.x, 0, pos.y);
        }
    }
}

export const availableAudioFrameworks = [
    new Volume(),
    new SpatialAudio()
];

export var audioFramework = availableAudioFrameworks[0];

export function switchAudioFramework(name: string) {
    if (!name || audioFramework.getName() == name) {
        return;
    }
    let old = audioFramework;
    audioFramework = availableAudioFrameworks.find(f => f.getName() == name);
    jitsiCafe.participants.forEach(p => {
        old.removeParticipant(p);
        audioFramework.addParticipant(p);
    });
    audioFramework.updateLocalPosition();
}

function getDistance(p1: { x: number, y: number }, p2: { x: number, y: number }) {
    return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

function getVolume(distance: number) {
    if (distance > settings.maxHearingDistance) {
        return 0;
    }
    return 1 - (distance / settings.maxHearingDistance);
}