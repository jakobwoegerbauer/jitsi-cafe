export enum EventType {
    KeyDown,
    KeyUp,
    WindowClick,
    MapClick
}

class InputHandler {
    private pressedKeys: Set<String> = new Set();
    private listeners: Map<EventType, ((e: Event)=>void)[]> = new Map();

    init() {
        window.addEventListener('keydown', this.onKeyDown.bind(this));
        window.addEventListener('keyup', this.onKeyUp.bind(this));
        window.addEventListener('click', this.onClick.bind(this));
    }

    addListener(type: EventType, listener: (e: Event) => void) {
        let list = this.listeners.get(type) ?? [];
        list.push(listener);
        this.listeners.set(type, list);
    }

    isKeyPressed(key: String) {
        return this.pressedKeys.has(key);
    }

    private dispatch(type: EventType, e: Event) {
        this.listeners.get(type)?.forEach(l => l(e));
    }

    private onKeyDown(e: KeyboardEvent) {
        this.pressedKeys.add(e.key);
        this.dispatch(EventType.KeyDown, e);
    }

    private onKeyUp(e: KeyboardEvent) {
        this.pressedKeys.delete(e.key);
        this.dispatch(EventType.KeyUp, e);
    }

    private onClick(e: KeyboardEvent) {
        this.pressedKeys.delete(e.key);
        this.dispatch(EventType.KeyUp, e);
    }
}

export const inputHandler = new InputHandler();