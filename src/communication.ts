import { JitsiConference, JitsiParticipant } from "../jitsiClassDef/jitsi";
import { jitsiCafe } from "./jitsi-cafe-room";
import { ParticipantStatus } from "./map-position";
import { RoomData } from "./maps/rooms";

export class Communication {
    private conference: JitsiConference;
    private handlers: Map<MessageType, (sender: JitsiParticipant, msg: Message) => void>;

    constructor(conference: JitsiConference) {
        this.conference = conference;
        this.conference.addConferenceListener('conference.endpoint_message_received', this._onEndpointMessage);
        this.handlers = new Map();
    }

    dispose() {
        this.conference.removeConferenceListener('conference.endpoint_message_received', this._onEndpointMessage);
    }

    _onEndpointMessage(participant: JitsiParticipant, payload: Message) {
        jitsiCafe.communication.onEndpointMessage(participant, payload);
    }

    onEndpointMessage(sender: JitsiParticipant, msg: Message) {
        if (msg.type && this.handlers.has(msg.type)) {
            this.handlers.get(msg.type)(sender, msg);
        }
    }

    sendTo(to: string, msg: Message, retries = 0, callback: ()=>void = null) {
        var rtc = this.conference._room.rtc;
        if (rtc._channel) {
            if(this.conference._room.rtc._channel.isOpen()) {
                this.conference.sendEndpointMessage(to, msg);
                if(callback) {
                    callback();
                }
            } else {
                if (retries > 0) {
                    rtc.addListener('rtc.data_channel_open', () => {
                        this.send(msg, retries-1, callback);
                    });
                }
            }
        } else {
            if(retries > 0) {
                setTimeout(() => this.send(msg, retries-1, callback), 100);
            }
        }
    }

    send(msg: Message, retries = 0, callback: ()=>void = null) {
        this.sendTo("", msg, retries, callback);
    }

    setHandler(type: MessageType, handler: (sender: JitsiParticipant, msg: Message) => void) {
        this.handlers.set(type, handler);
    }
}

export enum MessageType {
    STATUS = 'status',
    REQUEST_INIT_DATA = 'request_position',
    ROOM_DATA = 'room_data'
}

export abstract class Message {
    type: MessageType;

    constructor(type: MessageType) {
        this.type = type;
    }
}

export class StatusMessage extends Message {
    position: ParticipantStatus;
    isSpeaker: boolean;
    
    constructor(position: ParticipantStatus, isSpeaker: boolean) {
        super(MessageType.STATUS);
        this.position = position;
        this.isSpeaker = isSpeaker;
    }
}

export class RequestInitDataMessage extends Message {
    constructor() {
        super(MessageType.REQUEST_INIT_DATA);
    }
}

export class RoomDataMessage extends Message {
    roomData: RoomData;

    constructor(roomData: RoomData) {
        super(MessageType.ROOM_DATA);
        this.roomData = roomData;
    }
}