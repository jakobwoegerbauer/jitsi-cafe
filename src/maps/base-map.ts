import { CafeMap, TryMoveResult } from "./cafe-map";
import { Vec2 } from "../vec2";
import { MapPosition, ParticipantStatus } from "../map-position";
import { settings } from "../settings";
import { Participant } from "../participant";

export abstract class BaseMap implements CafeMap {
    private name: string;
    private spawn: ParticipantStatus;
    updateFn: () => void;

    constructor(name: string, spawn: ParticipantStatus, updateFn: () => void = null) {
        this.name = name;
        this.spawn = spawn;
        this.updateFn = updateFn;
    }

    getName() {
        return this.name;
    }

    getSpawn(participant: Participant) {
        return this.spawn;
    }

    update() {
        if(this.updateFn) {
            this.updateFn();
        }
    }

    abstract tryMove(from: MapPosition, vector: Vec2): TryMoveResult;

    abstract draw(ctx: CanvasRenderingContext2D): void;
}

export function crop(x: number, min: number, max: number) {
    return Math.max(min, Math.min(max, x));
}

export function tryMoveRectangle(rectPos: Vec2, rectSize: Vec2, from: MapPosition, vector: Vec2, linewidth: number = 0): TryMoveResult {
    let lw = new Vec2(linewidth, linewidth);
    let rad = new Vec2(settings.circleRadius, settings.circleRadius);

    let innerPos = Vec2.add(Vec2.add(rectPos, Vec2.scale(lw, 0.5)), rad);
    let innerSize = Vec2.subtract(Vec2.subtract(rectSize, lw), Vec2.scale(rad, 2));

    let outerPos = Vec2.subtract(Vec2.subtract(rectPos, Vec2.scale(lw, 0.5)), rad);
    let outerSize = Vec2.add(Vec2.add(rectSize, lw), Vec2.scale(rad, 2));

    let fromInInnerRect = isInRect(innerPos, innerSize, from);  
    let fromInOuterRect = isInRect(outerPos, outerSize, from, true);
    if (fromInInnerRect == isInRect(innerPos, innerSize, Vec2.add(from, vector))) {
        // from and to on same side of inner rectangle
        if (fromInOuterRect == isInRect(outerPos, outerSize, Vec2.add(from, vector), true)) {
            // from and to on same side of outer rectangle
            return TryMoveResult.default(from, vector);
        }
    }

    let originalTarget = MapPosition.move(from, vector);
    let target = MapPosition.clone(originalTarget);
    if (fromInInnerRect) {
        target = new MapPosition(
            crop(target.x, innerPos.x, innerPos.x + innerSize.x),
            crop(target.y, innerPos.y, innerPos.y + innerSize.y),
            from.rotation);
        return new TryMoveResult(from, target, Vec2.magnitude(Vec2.subtract(target, originalTarget)));
    } else if(!fromInOuterRect) {
        if (from.x < innerPos.x && vector.x > 0) {
            target.x = Math.min(target.x, outerPos.x);
        }
        if (from.x > innerPos.x + innerSize.x && vector.x < 0) {
            target.x = Math.max(target.x, outerPos.x + outerSize.x);
        }
        if (from.y < innerPos.y && vector.y > 0) {
            target.y = Math.min(target.y, outerPos.y);
        }
        if (from.y > innerPos.y + innerSize.y && vector.y < 0) {
            target.y = Math.max(target.y, outerPos.y + outerSize.y);
        }
        return new TryMoveResult(from, target, Vec2.magnitude(Vec2.subtract(target, originalTarget)));
    } else {
        // moving away from line
        return TryMoveResult.default(from, vector);
    }
}

export function isInRect(rectPos: Vec2, rectSize: Vec2, pos: Vec2, strongInside = false) {
    let end = Vec2.add(rectPos, rectSize);
    if (strongInside) {
        return pos.x > rectPos.x && pos.x < end.x && pos.y > rectPos.y && pos.y < end.y;
    } else {
        return pos.x >= rectPos.x && pos.x <= end.x && pos.y >= rectPos.y && pos.y <= end.y;
    }
}

export abstract class ConstrainedMap extends BaseMap {
    size: Vec2;

    constructor(name: string, size: Vec2, spawn: ParticipantStatus) {
        super(name, spawn);
        this.size = size;
    }

    tryMove(from: MapPosition, vector: Vec2): TryMoveResult {
        return tryMoveRectangle(Vec2.zero(), this.size, from, vector);
    }

    draw(ctx: CanvasRenderingContext2D) {
        ctx.save();

        ctx.save();
        this.drawInnerMap(ctx);
        ctx.restore();

        ctx.lineWidth = 4;
        ctx.strokeRect(0, 0, this.size.x, this.size.y);
        ctx.restore();
    }

    abstract drawInnerMap(ctx: CanvasRenderingContext2D): void;
}