import { MapPosition, ParticipantStatus } from "../map-position";
import { Vec2 } from "../vec2";
import { ConstrainedMap } from "./base-map";

export class SimpleMap extends ConstrainedMap {
    constructor() {
        super("Empty room", new Vec2(1500, 1100), new ParticipantStatus(70, 100, 0, ""));
    }

    drawInnerMap(ctx: CanvasRenderingContext2D): void {
        // empty map
    }
}