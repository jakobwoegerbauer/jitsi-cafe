/*import { MapPosition } from "../position";
import { Drawable, TryMoveResult, Vec2 } from "./cafe-map"

export abstract class MapPolygon implements Drawable {
    path: Path2D;
    topLeft: Vec2;
    bottomRight: Vec2;

    constructor(path: Path2D, topLeft: Vec2, bottomRight: Vec2) {
        this.path = path;
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
    }

    abstract draw(ctx: CanvasRenderingContext2D): void;
}

export class Wall extends MapPolygon {
    color: string;
    fillColor: string;
    lineWidth: number;

    constructor(path: Path2D, topLeft: Vec2, bottomRight: Vec2, color: string = "black", fillColor: string = null, lineWidth: number = 10) {
        super(path, topLeft, bottomRight);
        this.color = color;
        this.fillColor = fillColor;
        this.lineWidth = lineWidth;
    }

    draw(ctx: CanvasRenderingContext2D): void {
        ctx.save();
        ctx.beginPath();
        ctx.strokeStyle = this.color;
        ctx.lineWidth = this.lineWidth;
        ctx.stroke(this.path);
        if (this.fillColor) {
            ctx.fillStyle = this.fillColor;
            ctx.fill(this.path);
        }
        ctx.restore();
    }
}

export class Teleporter {
    position: Vec2;
    size: Vec2;
    teleportPosition: MapPosition;

    constructor(position: Vec2, size: Vec2, teleportPosition: MapPosition) {
        this.position = position;
        this.size = size;
        this.teleportPosition = teleportPosition;
    }

    draw(ctx: CanvasRenderingContext2D): void {
        ctx.strokeStyle = "yellow";
        ctx.fillRect(this.position.x, this.position.y, this.size.x, this.size.y);
    }
}

export type PolygonParameter = Vec2 | MapPolygon;

const CONTEXT_MARGIN = 100;

export class Room implements Drawable {
    private roomContext = document.createElement("canvas").getContext("2d");
    areaId: number;
    position: Vec2;
    children: Room[] = [];
    polygons: MapPolygon[] = [];
    topLeft = new Vec2(Infinity, Infinity);
    bottomRight = Vec2.zero();

    constructor(position: Vec2 = Vec2.zero(), areaId: number = 0) {
        this.position = position;
        this.areaId = areaId;
    }

    private _updateContext() {
        let size = Vec2.subtract(this.bottomRight, this.topLeft);
        this.roomContext.canvas.width = size.x + 2 * CONTEXT_MARGIN;
        this.roomContext.canvas.height = size.y + 2 * CONTEXT_MARGIN;
        this.roomContext.clearRect(0, 0, size.x, size.y);
        this.roomContext.save();
        this.roomContext.translate(-this.position.x + CONTEXT_MARGIN, -this.position.y + CONTEXT_MARGIN);
        this.polygons.forEach(p => p.draw(this.roomContext));
        this.roomContext.restore();
    }

    addChildren(...polygons: Room[]) {
        polygons.forEach(p => {
            this.children.push(p);
            this.topLeft = Vec2.min(this.topLeft, Vec2.add(p.position, p.topLeft));
            this.bottomRight = Vec2.max(this.bottomRight, Vec2.add(p.position, p.bottomRight));
        });
        this._updateContext();
    }

    addPolygon(polygon: MapPolygon) {
        this.polygons.push(polygon);
        this.topLeft = Vec2.min(this.topLeft, polygon.topLeft);
        this.bottomRight = Vec2.max(this.bottomRight, polygon.bottomRight);
        this._updateContext();
    }

    isInRoom(position: MapPosition): boolean {
        return this.children.some(c => c.isInRoom(position))
            || (position.areaId == this.areaId && this.roomContext.isPointInPath(position.x, position.y));
    }

    tryMove(from: MapPosition, vector: Vec2): TryMoveResult {
        var result = new TryMoveResult(from, vector);

        this.children.forEach(c => {
            let newPos = c.tryMove(from, vector);
            result = TryMoveResult.first(result, newPos);
        });

        let length = Vec2.magnitude(vector);
        let vec = Vec2.scale(vector, 1 / (2 * length));
        let pos = Vec2.clone(from);
        for (let i = 1; i <= length; i++) {
            pos = Vec2.add(pos, Vec2.scale(vec, i));
            this.polygons.forEach(p => {
                if (this.roomContext.isPointInStroke(p.path, pos.x, pos.y)) {
                    console.log("collision!");
                    result = TryMoveResult.first(result, new TryMoveResult(from, Vec2.scale(vec, i - 1), length));
                }
            })
        }

        return result;
    }

    draw(ctx: CanvasRenderingContext2D): void {
        ctx.save();
        ctx.translate(this.position.x, this.position.y);
        this.polygons.forEach(p => p.draw(ctx));
        this.children.forEach(c => c.draw(ctx));
        ctx.restore();
    }
}

*/