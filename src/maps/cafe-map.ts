import { MapPosition, ParticipantStatus } from "../map-position";
import { Participant } from "../participant";
import { Vec2 } from "../vec2";

export interface Drawable {
    draw(ctx: CanvasRenderingContext2D): void;
}

export interface Interactable {
    tryMove(from: MapPosition, vector: Vec2, currentTranslation: Vec2): TryMoveResult;
}

export interface CafeMap extends Drawable, Interactable {
    getName(): string;
    getSpawn(p: Participant): ParticipantStatus;
    update(): void;
}

export class TryMoveResult {
    position: MapPosition;
    targetPosition: MapPosition;
    intersecDist: number;

    constructor(position: MapPosition, targetPosition: MapPosition, intersecDist: number = Infinity) {
        this.position = position;
        this.targetPosition = targetPosition;
        this.intersecDist = intersecDist;
    }

    static default(position: MapPosition, vector: Vec2): TryMoveResult {
        let target = Vec2.add(position, vector);
        return new TryMoveResult(MapPosition.clone(position), new MapPosition(target.x, target.y, position.rotation));
    }

    static first(r1: TryMoveResult, r2: TryMoveResult) {
        return r1.intersecDist < r2.intersecDist ? r1 : r2;
    }
}