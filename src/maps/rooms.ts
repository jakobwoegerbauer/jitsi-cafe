import { jitsiCafe } from "../jitsi-cafe-room";
import { MapPosition, ParticipantStatus } from "../map-position";
import { Participant } from "../participant";
import { settings } from "../settings";
import { ui } from "../ui/ui";
import { Vec2 } from "../vec2";
import { isInRect, tryMoveRectangle } from "./base-map";
import { CafeMap, Drawable, Interactable, TryMoveResult } from "./cafe-map";

interface MapPart extends Drawable, Interactable {
    update(): void;
}

class MapPartWrapper {
    position: Vec2;
    drawBefore: boolean;
    mapPart: MapPart;
    parent: ParentMapPart;

    constructor(parent: ParentMapPart, part: MapPart, position: Vec2, drawLater: boolean) {
        this.parent = parent;
        this.mapPart = part;
        this.position = position;
        this.drawBefore = drawLater;
    }
}

abstract class ParentMapPart implements MapPart {
    subParts: MapPartWrapper[] = [];
    updateFn: () => void;

    addMapPart(part: MapPart, position: Vec2, drawLater = false) {
        this.subParts.push(new MapPartWrapper(this, part, position, drawLater));
    }

    getWrappers(part: MapPart) {
        return this.subParts.filter(p => p.mapPart == part);
    }

    tryMove(from: MapPosition, vector: Vec2, currentTranslation: Vec2): TryMoveResult {
        let res = TryMoveResult.default(from, vector);
        this.subParts.forEach(p => {
            res = TryMoveResult.first(res, p.mapPart.tryMove(from, vector, Vec2.add(currentTranslation, p.position)));
        });
        return res;
    }

    update() {
        this.subParts.forEach(p => p.mapPart.update());
        if (this.updateFn) {
            this.updateFn();
        }
    }

    abstract draw(ctx: CanvasRenderingContext2D): void;

    drawBefore(ctx: CanvasRenderingContext2D): void {
        this.subParts.filter(p => p.drawBefore).forEach(p => this.drawSub(ctx, p));
    }

    drawAfter(ctx: CanvasRenderingContext2D): void {
        this.subParts.filter(p => !p.drawBefore).forEach(p => this.drawSub(ctx, p));
    }

    private drawSub(ctx: CanvasRenderingContext2D, p: MapPartWrapper) {
        ctx.save();
        ctx.translate(p.position.x, p.position.y);
        p.mapPart.draw(ctx);
        ctx.restore();
    }
}

class RectangularRoom extends ParentMapPart {
    size: Vec2;
    wallThickness: number;
    strokeStyle: string;
    fillStyle: string;
    fillAlpha: number = 1;
    wallSize: Vec2;
    clearBackground: boolean;

    isInRoom = false;
    onIsInRoomChange: (isInRoom: boolean) => void;

    constructor(size: Vec2, wallThickness: number = 15) {
        super();
        this.size = size;
        this.wallThickness = wallThickness;
        this.wallSize = new Vec2(wallThickness, wallThickness);
    }

    updateIsInRoom(pos: Vec2) {
        let res = isInRect(Vec2.diag(settings.circleRadius + this.wallThickness / 2),
            Vec2.subtract(this.size, Vec2.diag(2 * settings.circleRadius + this.wallThickness)), pos);
        if (res != this.isInRoom) {
            this.isInRoom = res;
            if (this.onIsInRoomChange) {
                this.onIsInRoomChange(this.isInRoom);
            }
        }
    }

    tryMove(from: MapPosition, vector: Vec2, currentTranslation: Vec2): TryMoveResult {
        let res = TryMoveResult.first(
            tryMoveRectangle(currentTranslation, this.size, from, vector, this.wallThickness),
            super.tryMove(from, vector, currentTranslation));
        this.updateIsInRoom(Vec2.subtract(res.targetPosition, currentTranslation))
        return res;
    }

    draw(ctx: CanvasRenderingContext2D): void {
        if (this.clearBackground) {
            ctx.save();
            ctx.globalCompositeOperation = "destination-out";
            ctx.fillStyle = "black";
            ctx.fillRect(0, 0, this.size.x, this.size.y);
            ctx.restore();
        }

        super.drawBefore(ctx);
        ctx.save();
        if (this.fillStyle) {
            ctx.save();
            ctx.globalAlpha *= this.fillAlpha;
            ctx.fillStyle = this.fillStyle;
            ctx.fillRect(0, 0, this.size.x, this.size.y);
            ctx.restore();
        }
        if (this.wallThickness && this.strokeStyle) {
            ctx.lineWidth = this.wallThickness;
            ctx.strokeStyle = this.strokeStyle;
            ctx.strokeRect(0, 0, this.size.x, this.size.y);
        }
        ctx.restore();
        super.drawAfter(ctx);
    }
}

export class Teleporter implements MapPart {
    size: Vec2;
    teleportPosition: ParticipantStatus;

    constructor(size: Vec2, teleportPosition: ParticipantStatus) {
        this.size = size;
        this.teleportPosition = teleportPosition;
    }

    tryMove(from: MapPosition, vector: Vec2, currentTranslation: Vec2): TryMoveResult {
        let res = tryMoveRectangle(currentTranslation, this.size, from, vector, 0);
        if (isInRect(currentTranslation, this.size, from)) {
            if (this.teleportPosition.areaId != undefined) {
                jitsiCafe.participantSelf.position.areaId = this.teleportPosition.areaId;
            }
            return new TryMoveResult(from, MapPosition.clone(this.teleportPosition), 0);
        }
        return TryMoveResult.default(from, vector);
    }

    draw(ctx: CanvasRenderingContext2D): void {
        ctx.save();
        ctx.fillStyle = "yellow";
        ctx.globalAlpha *= 0.2;
        ctx.fillRect(0, 0, this.size.x, this.size.y);
        ctx.restore();
    }

    update() { }
}

export class Door implements MapPart {
    mvPosition: Vec2;
    mvSize: Vec2;
    drawPath: Path2D;
    wallDirection: Vec2;

    constructor(wallDirection: Vec2, size: number, wallThickness: number) {
        this.wallDirection = wallDirection;
        let dir = Vec2.uniform(wallDirection);
        let centerStart = Vec2.scale(dir, size / 2 - settings.circleRadius);
        let centerEnd = Vec2.scale(centerStart, -1);
        let cornerDir = Vec2.rotate(dir, Math.PI / 2);
        let cornerVec = Vec2.scale(cornerDir, settings.circleRadius + wallThickness / 2 + 1);
        let c1 = Vec2.add(centerStart, cornerVec);
        let c2 = Vec2.subtract(centerStart, cornerVec);
        let c3 = Vec2.add(centerEnd, cornerVec);
        let c4 = Vec2.subtract(centerEnd, cornerVec);
        this.mvPosition = Vec2.min(Vec2.min(c1, c2), Vec2.min(c3, c4));
        this.mvSize = Vec2.subtract(Vec2.max(Vec2.max(c1, c2), Vec2.max(c3, c4)), this.mvPosition);

        let sideVec = Vec2.scale(cornerDir, wallThickness / 2);
        c1 = Vec2.add(Vec2.scale(dir, size / 2), sideVec);
        c2 = Vec2.subtract(Vec2.scale(dir, size / 2), sideVec);
        c3 = Vec2.subtract(Vec2.scale(dir, -size / 2), sideVec);
        c4 = Vec2.add(Vec2.scale(dir, -size / 2), sideVec);

        this.drawPath = new Path2D();
        this.drawPath.moveTo(c1.x, c1.y);
        this.drawPath.lineTo(c2.x, c2.y);
        this.drawPath.lineTo(c3.x, c3.y);
        this.drawPath.lineTo(c4.x, c4.y);
        this.drawPath.closePath();
    }

    tryMove(from: MapPosition, vector: Vec2, currentTranslation: Vec2): TryMoveResult {
        if (isInRect(Vec2.add(currentTranslation, this.mvPosition), this.mvSize, Vec2.add(from, vector))) {
            return new TryMoveResult(from, MapPosition.move(from, new Vec2(vector.x * this.wallDirection.y, vector.y * this.wallDirection.x)), 0);
        }
        return TryMoveResult.default(from, vector);
    }

    draw(ctx: CanvasRenderingContext2D): void {
        ctx.save();
        ctx.globalCompositeOperation = "destination-out";
        ctx.fillStyle = "black";
        ctx.fill(this.drawPath);
        ctx.restore();
    }

    update() { }
}

export class RoomMap extends RectangularRoom implements CafeMap {
    name: string;
    spawn: ParticipantStatus;
    rooms: MapPartWrapper[] = [];

    constructor(name: string, size: Vec2, spawn: ParticipantStatus, wallThickness = 20) {
        super(size, wallThickness);
        this.name = name;
        this.spawn = spawn;
    }

    tryMove(from: MapPosition, vector: Vec2, currentTranslation: Vec2): TryMoveResult {
        return super.tryMove(from, vector, currentTranslation);
    }

    getName(): string {
        return this.name;
    }

    getSpawn(p: Participant): ParticipantStatus {
        return this.spawn;
    }
}

class MapText implements MapPart {
    align: CanvasTextAlign = "center";
    baseline: CanvasTextBaseline = "middle";
    direction: CanvasDirection = "ltr";
    font: string = "28px serif";
    maxWidth?: number;
    strokeStyle: string = null; // = "black";
    fillStyle: string = "black";
    stayStraight: boolean = false;
    rotation = 0;
    text: string;

    constructor(text: string) {
        this.text = text;
    }

    draw(ctx: CanvasRenderingContext2D): void {
        ctx.save();
        ctx.rotate(this.rotation);
        if (this.stayStraight) {
            ctx.rotate(jitsiCafe.participantSelf.position.rotation);
        }
        ctx.textAlign = this.align;
        ctx.textBaseline = this.baseline;
        ctx.direction = this.direction;
        ctx.font = this.font;
        if (this.strokeStyle) {
            ctx.strokeStyle = this.strokeStyle;
            ctx.lineWidth = 10;
            ctx.strokeText(this.text, 0, 0, this.maxWidth);
        }
        if (this.fillStyle) {
            ctx.fillStyle = this.fillStyle;
            ctx.fillText(this.text, 0, 0, this.maxWidth);
        }
        ctx.restore();
    }

    tryMove(from: MapPosition, vector: Vec2, currentTranslation: Vec2): TryMoveResult {
        return TryMoveResult.default(from, vector);
    }

    update() { }
}

export class RoomData {
    roomName: string;
    gameName: string;
    gameLink: string;

    constructor(roomName: string, gameName: string, gameLink: string) {
        this.roomName = roomName;
        this.gameName = gameName;
        this.gameLink = gameLink;
    }
}

export const roomData: Map<string, RoomData> = new Map();

export function createRoomMap(): CafeMap {

    let posMainRoom = new Vec2(50000, 50000);
    let posSpawnRoom = new Vec2(1500, 1500);

    let map = new RoomMap("Rooms", new Vec2(100000, 100000), null, 20);
    map.getSpawn = (participant) => {
        return ParticipantStatus.of(Vec2.add(posSpawnRoom, Vec2.diag(150)), 0, participant.id);
    }

    let mainRoom = new RectangularRoom(new Vec2(2500, 2500));
    mainRoom.strokeStyle = "black";
    mainRoom.onIsInRoomChange = (isInRoom) => {
        if (isInRoom) {
            settings.stillstandFadeout = true;
        }
    }
    let gamesIndicator = new MapText("Gamerooms -->");
    gamesIndicator.fillStyle = "black";
    gamesIndicator.rotation = 0.6;
    mainRoom.addMapPart(gamesIndicator, new Vec2(400, 1200));
    map.addMapPart(mainRoom, posMainRoom);

    let spawnWrapper = new RectangularRoom(new Vec2(3000, 3000), 0);
    spawnWrapper.fillAlpha = 0.2;
    spawnWrapper.fillStyle = "white";
    map.addMapPart(spawnWrapper, Vec2.zero());

    let spawnRoom = new RectangularRoom(new Vec2(700, 300), 15);
    spawnRoom.strokeStyle = "black";
    let t = new Teleporter(new Vec2(250, 250), ParticipantStatus.of(Vec2.add(new Vec2(300, 1000), posMainRoom), 0, ""));
    spawnRoom.addMapPart(t, new Vec2(425, 25));

    let join = new MapText("Join!");
    join.font = "38px serif";
    join.align = "center";
    join.baseline = "middle";
    join.stayStraight = true;
    spawnRoom.addMapPart(join, new Vec2(550, 150));

    spawnWrapper.addMapPart(spawnRoom, posSpawnRoom);

    let welcomeText = new MapText("Welcome to Jitsi-Café!");
    welcomeText.font = "38px serif";
    let intrText = new MapText("You can only hear and be heared by people in the red circle");
    let moveText = new MapText("You can move around with the arrows keys on your keyboard");
    let tutorialText = new MapText("More details and settings can be found in the top left corner");
    spawnRoom.addMapPart(welcomeText, new Vec2(150, -200));
    spawnRoom.addMapPart(intrText, new Vec2(150, -130));
    spawnRoom.addMapPart(moveText, new Vec2(150, -100));
    spawnRoom.addMapPart(tutorialText, new Vec2(150, -70));

    let bottomText1 = new MapText("Zoom in/out with +/- and rotate with ,/.");
    let tutorialText2 = new MapText("On a mobile device? Check settings -> joystick for mobile devices");
    spawnRoom.addMapPart(bottomText1, new Vec2(150, 350));
    spawnRoom.addMapPart(tutorialText2, new Vec2(150, 380));

    let joinText = new MapText("Move to the right to join the others!");
    joinText.align = "center";
    spawnRoom.addMapPart(joinText, new Vec2(550, -30));

    let createGameRoom = function (name: string, color: string) {
        let gameRoom = new RectangularRoom(new Vec2(600, 600), 4);
        gameRoom.clearBackground = true;
        gameRoom.strokeStyle = color;
        let door = new Door(new Vec2(1, 0), 300, 4);
        gameRoom.addMapPart(door, new Vec2(300, 0));
        let header = new MapText(name);
        header.align = "center";
        header.baseline = "bottom";
        header.fillStyle = color;
        gameRoom.addMapPart(header, new Vec2(300, -15));

        let description = new MapText("");
        description.fillStyle = color;
        description.update = () => {
            let cnt = jitsiCafe.allParticipants.filter(p => p.position.areaId == name).length;
            let rd = roomData.get(name);
            let playingText = rd.gameName ? ` playing '${rd.gameName}'` : ' in the room';
            if (cnt == 0 || jitsiCafe.participantSelf.position.areaId == name)
                description.text = "";
            else if (cnt == 1)
                description.text = `1 person is ${playingText}`;
            else
                description.text = `${cnt} people are ${playingText}`;
        };
        gameRoom.addMapPart(description, new Vec2(300, 100));

        gameRoom.onIsInRoomChange = isInRoom => {
            let w = gameRoom.getWrappers(header)[0];
            if (isInRoom) {
                jitsiCafe.participantSelf.position.areaId = name;
                w.position = Vec2.add(w.position, new Vec2(0, -70));
                gameRoom.fillStyle = null;
                map.fillStyle = "black";
                map.fillAlpha = 0.3;
                ui.header.roomView.setRoom(roomData.get(name));
            } else {
                jitsiCafe.participantSelf.position.areaId = "";
                w.position = Vec2.add(w.position, new Vec2(0, 70));
                gameRoom.fillStyle = "black";
                gameRoom.fillAlpha = 0.3;
                map.fillStyle = null;
                ui.header.roomView.disable();
            }
        }
        roomData.set(name, new RoomData(name, "", ""));
        return gameRoom;
    }

    mainRoom.addMapPart(createGameRoom("Gameroom 1", "yellow"), new Vec2(300, 1600));
    mainRoom.addMapPart(createGameRoom("Gameroom 2", "blue"), new Vec2(1200, 1600));

    //let d = new Door(new Vec2(0, 1), 150, spawnRoom.wallThickness);
    //spawnRoom.addMapPart(d, new Vec2(700, 100));
    console.log(map);
    return map;
}