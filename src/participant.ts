import { APP, JitsiParticipant, JitsiTrack } from "../jitsiClassDef/jitsi";
import { audioFramework } from "./audio";
import { jitsiCafe } from "./jitsi-cafe-room";
import { Vec2 } from "./vec2";
import { MapPosition, ParticipantStatus } from "./map-position";
import { settings } from "./settings";
import { videoFramework } from "./videoFramework/video-framework";

export abstract class Participant {
    position: ParticipantStatus = new ParticipantStatus(0, 0, 0, "");
    color: string;
    id: string;
    isSpeaker: boolean = false; // if true everyone should hear this participant
    alpha: number;

    constructor(color: string, id: string) {
        this.color = color;
        this.id = id;
    }

    abstract updatePosition(newPos: Vec2): void;

    abstract getName(): string;

    abstract getAudioLevel(): number;

    abstract getVideoTrack(): JitsiTrack;

    draw(ctx: CanvasRenderingContext2D) {
        ctx.save();
        ctx.globalAlpha *= this.alpha;
        ctx.beginPath();
        ctx.strokeStyle = this.color;
        ctx.lineWidth = Math.ceil(this.getAudioLevel() * 10);
        ctx.arc(0, 0, settings.circleRadius, 0, 2 * Math.PI);
        ctx.stroke();

        ctx.save();
        ctx.globalAlpha *= 0.3;
        if (this.isSpeaker) {
            ctx.fillStyle = "rgba(0, 255, 0, 0.8)";
        } else {
            ctx.fillStyle = "rgba(255, 255, 255, 0.8)";
        }
        ctx.fill();
        ctx.restore();

        ctx.font = "20px Arial";
        ctx.lineWidth = 1;
        ctx.textAlign = "center";
        ctx.fillStyle = "black";

        ctx.fillText(this.getName(), 0, 0);
        ctx.restore();
    };
}

export class RemoteParticipant extends Participant {
    jitsiParticipant: JitsiParticipant;

    constructor(color: string, id: string, jitsiParticipant: JitsiParticipant) {
        super(color, id);
        this.jitsiParticipant = jitsiParticipant;
    }

    updatePosition(position: ParticipantStatus) {
        if(Vec2.magnitude(Vec2.subtract(this.position, position)) >= 1) {
            this.alpha = 1;
        }
        this.position = position;
        audioFramework.updateRemotePosition(this);
        videoFramework.updateRemotePosition(this);
    };

    getName(): string {
        return APP.conference.getParticipantDisplayName(this.id)
    }

    _getTrackByMedia(media: string) {
        var tracks = this.jitsiParticipant.getTracksByMediaType(media);
        if (tracks && tracks[0]) {
            return tracks[0];
        }
        return null;
    }

    getAudioTrack(): JitsiTrack {
        return this._getTrackByMedia("audio");
    }

    getVideoTrack(): JitsiTrack {
        return this._getTrackByMedia("video");
    }

    getAudioLevel(): number {
        return this.getAudioTrack()?.audioLevel;
    }

    setSpeaker(enable: boolean) {
        this.isSpeaker = enable;
        audioFramework.updateRemotePosition(this);
    }
}

export class LocalParticipant extends Participant {
    constructor(color: string, id: string) {
        super(color, id);
    }

    move(vec: Vec2) {
        this.updatePosition(ParticipantStatus.move(this.position, jitsiCafe.map.tryMove(this.position, vec, Vec2.zero()).targetPosition));
        this.resetAlpha();
    };

    rotate(angle: number) {
        this.position.rotation = (this.position.rotation + angle) % (2 * Math.PI);
        audioFramework.updateLocalPosition();
        videoFramework.updateLocalPosition();
        this.resetAlpha();
    }

    private resetAlpha() {
        this.alpha = 1;
        jitsiCafe.participants.forEach(p => p.alpha = 1);
        jitsiCafe.mapAlpha = 1;
    }

    updatePosition(position: ParticipantStatus) {
        this.position = position;
        audioFramework.updateLocalPosition();
        videoFramework.updateLocalPosition();
    }

    getName(): string {
        return APP.conference.getLocalDisplayName();
    }

    getAudioLevel(): number {
        if (jitsiCafe.conference._room.getLocalAudioTrack()) {
            return jitsiCafe.conference._room.getLocalAudioTrack().audioLevel;
        }
        return 0;
    }

    getVideoTrack() {
        return APP.conference._room.getLocalVideoTrack();
    }

    draw(ctx: CanvasRenderingContext2D) {
        ctx.save();
        super.draw(ctx);
        ctx.globalAlpha *= this.alpha;
        ctx.beginPath();
        ctx.strokeStyle = settings.maxHearingDistanceColor;
        ctx.lineWidth = 1.0;
        if (!this.isSpeaker) {
            ctx.arc(0, 0, settings.maxHearingDistance, 0, 2 * Math.PI);
        }
        ctx.stroke();
        ctx.restore();
    }

    setSpeaker(enable: boolean) {
        this.isSpeaker = enable;
    }
}